#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="gnomba"

(test -f configure.in \
  && test -f $srcdir/src/gnomba.c) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gnomba directory"
    exit 1
}

. $srcdir/macros/autogen.sh
