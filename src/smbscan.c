/* Gnomba
 * Copyright (C) 1999 Gnomba Team
 *
 * smbscan.c
 *
 * Portions of this code based on gtksamba.c from GtkNeighborhood. Portions
 * of this code based on client.c from Samba.  This code was originally 
 * submitted by Michel Johnson, it has since be _heavily_ modifed by me.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <samba/include/includes.h>
#include "smbscan.h"
#include "smbwrap.h"
#include "gnomba.h"

extern struct in_addr ipzero; /*I can only assume that this is declared in samba*/

static struct in_addr find_master() {
	int count;
	int fd = -1;
	struct in_addr *ip_list;
	struct in_addr dest_ip;

	dest_ip = *iface_bcast(ipzero);
	fd = open_socket_in(SOCK_DGRAM, 0, 3, interpret_addr("0.0.0.0"), True);
	if (fd == -1) 
		return dest_ip;
	set_socket_options(fd,"SO_BROADCAST");
	ip_list = name_query(fd, "\01\02__MSBROWSE__\02", 1, True, True, dest_ip, &count, NULL);
	if (count) 
		dest_ip = *ip_list;
	return dest_ip;
}

struct cli_state *makeConnection(gchar *server, gchar *share, gchar * name, gchar * pass, struct in_addr * dest_ip) {
	struct cli_state *c;
	struct nmb_name called, calling;
	char *server_n;
	struct in_addr ip;
	extern struct in_addr ipzero;
	extern pstring global_myname;

	static pstring password;
	static pstring username;
	static pstring workgroup;
	static int name_type = 0x20;
	static int port = SMB_PORT;

	server_n = server;
	make_nmb_name(&calling, global_myname, 0x0, "");
	make_nmb_name(&called , server, name_type, "");

	ip = ipzero;
	if ((!strcmp(server, "*SMBSERVER"))&&(dest_ip != NULL))
		ip = *dest_ip;
	
	/* have to open a new connection */
	if (!(c=cli_initialise(NULL)) || (cli_set_port(c, port) == 0) ||
			!cli_connect(c, server_n, &ip)) {
		if (debug) g_print("Connection to %s failed\n", server_n);
		return NULL;
	}

	if (!cli_session_request(c, &calling, &called)) {
		 if (debug) g_print("session request to %s failed\n", called.name);
		cli_shutdown(c);
		return NULL;
	}
	
	if (!cli_negprot(c)) {
		 if (debug) g_print(("protocol negotiation failed\n"));
		cli_shutdown(c);
		return NULL;
	}
				
	/* set username and password */
 	if (name != NULL) pstrcpy(username, name);
 	if (pass != NULL) pstrcpy(password, pass);
	
	if (!cli_session_setup(c, username, password, strlen(password),
												 password, strlen(password), workgroup)) {
		if (debug) g_print("session setup failed: %s\n", cli_errstr(c));
		return NULL;
	}

	if (!cli_send_tconX(c, share, "?????", password, strlen(password)+1)) {
		 if (debug) g_print("tree connect failed: %s\n", cli_errstr(c));
		cli_shutdown(c);
		return NULL;
	}
	
	if (debug) g_print(" session request to %s ok\n", called.name);

	return c;
}

machineNode *scan_neighborhood(machineNode *curr) {
	machineNode *root = curr;
	machineNode *groups = NULL, *tmpgroups;

	//get the list of groups
	groups = scan_workgroups(groups);
	while (groups) {
		if (ScanAll||(strcmp(groups->workgroupName, DefaultWorkgroup) == 0)) {
			if (debug) g_print("scanning for machines in %s\n",groups->workgroupName);
			curr = scan_servers(groups->workgroupName,curr);
			tmpgroups= groups->next;
			g_free(groups->workgroupName);
			g_free(groups);
			groups = tmpgroups;
		} else {
			tmpgroups = groups->next;
			curr->next = groups;
			curr = groups;
			groups = tmpgroups;
		}
	}
	return root;
}

machineNode *scan_workgroups(machineNode *root) {
	machineNode *curr = root;
	static struct cli_state *cli;
	struct in_addr dest_ip; 

	//EVIL HACK we need to have curr updated, but this function has to have this signature ugly
	void list_group(const char *name, uint32 m, const char *comment) {
		machineNode *node = g_malloc0(sizeof(*node));
		if (curr)
			curr->next = node;
		else
			root = node;
		curr = node;
		curr->workgroupName = g_strdup(name);
		g_strup(curr->workgroupName);
		if (debug) g_print("workgroup:%s\n",name);
	}

	charset_initialise();  //no idea, but this was done so I guess we need to do it.
	load_interfaces();	//dito
	dest_ip = find_master();
	cli = makeConnection("*SMBSERVER", "IPC$",NULL,NULL, &dest_ip);
	if (!cli)
		return root;
	cli_NetServerEnum(cli,cli->server_domain,SV_TYPE_DOMAIN_ENUM,list_group);
	cli_shutdown(cli);
	
	return root;
}

machineNode *scan_servers(char *workgroup, machineNode *root) {
	machineNode *curr = root;
	static struct cli_state *cli;
	struct in_addr dest_ip; 

	void list_server(const char *name, uint32 m, const char *comment) {
		machineNode *node = g_malloc0(sizeof(*node));
		if (curr)
			curr->next = node;
		else
			root = node;
		curr = node;
		curr->workgroupName = g_strdup(workgroup);
		curr->machineName = g_strdup(name);
		if (DefaultUser && *DefaultUser)
			curr->username = g_strdup(DefaultUser);
		curr->ip = g_malloc0(sizeof(gchar) * 16);
		curr->scanned = FALSE;
		g_print("machine found: %s\n",name);
	}
	
	dest_ip = find_master();
	cli = makeConnection("*SMBSERVER", "IPC$",NULL, NULL, &dest_ip);
	if (!cli)
		return curr;
	cli_NetServerEnum(cli,workgroup,SV_TYPE_ALL,list_server);
	cli_shutdown(cli);

	return curr;
}

int getSmbShareList(machineNode *curr) {
	static struct cli_state *cli;

	if (curr->username == NULL) {
		cli = makeConnection(curr->machineName, "IPC$", NULL, NULL, NULL);
		if (!cli) return -5;	 
	} else {
		cli = makeConnection(curr->machineName, "IPC$", curr->username, curr->passwd, NULL);
		if (!cli) return -5;	 
	}
	
	gtk_clist_freeze(GTK_CLIST(workgrouplist));

	{
		machineNode *save = currnode;
		currnode = curr;
		cli_RNetShareEnum(cli, addShare);
		currnode = save;
	}

	cli_shutdown(cli);
	
	gtk_clist_thaw(GTK_CLIST(workgrouplist));
	gtk_clist_columns_autosize(GTK_CLIST(workgrouplist));
	return 0;
}
