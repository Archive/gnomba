/* gnomba - Gnome Samba Browser
 * Copyright (C) 1999 Chris Rogers, Brian Nigito
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#ifndef __ERROR_H__
#define __ERROR_H__

#include "gnomba.h"

void ShowError(gchar *emsg);


#endif /* __ERROR_H__ */


/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
