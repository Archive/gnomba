/* Gnomba
 * Copyright (C) 1999 Gnomba Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#ifndef __SMBSCAN_H__
#define __SMBSCAN_H__

#include "gnomba.h"

machineNode *scan_neighborhood(machineNode *curr);

machineNode *scan_workgroups(machineNode *root);

machineNode *scan_servers(char *workgroup, machineNode *root);

int getSmbShareList();
				
#endif // not defined __SMBSCAN_H__
