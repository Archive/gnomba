/* Gnomba
 * Copyright (C) 1999 Gnomba Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

/* smbwrap.h - all of the smb related functions */


#ifndef __SMBWRAP_H__
#define __SMBWRAP_H__

#include "gnomba.h"
#if !defined(uint32) && !defined(HAVE_UINT32_FROM_RPC_RPC_H)
#if (SIZEOF_INT == 4)
#define uint32 unsigned int
#elif (SIZEOF_LONG == 4)
#define uint32 unsigned long
#elif (SIZEOF_SHORT == 4)
#define uint32 unsigned short
#else
#define uint32 unsigned int
#endif
#endif
				
int getShareList(machineNode * tmp);
void browseShare();
int doMount();
void refreshList();
void unmountShare();
void DoScan();
int doCommand();
void addNetworkPrinter(); 
void addSmbPrinter(); 
void removePrinter();
void unmountAllShares();
void addShare(const char *name, uint32 type, const char *comment);
				
#endif /* __SMBWRAP_H__ */

/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
