/* Gnomba - Gnome Samba Browser
 * Copyright (C) 1999 Gnomba Team
 *
 * gnomba.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *	
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the	
 * GNU General Public License for more details.
 *	
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	
 * 02111-1307, USA.
 */

#include <sys/param.h>
#include "config.h"
#include <gnome.h>
#include <ctype.h>
#include "gnomba.h"
#include "menu.h"
#include "browser.h"
#include "statusbar.h"
#include "net.h"
#include "error.h"
#include "smbwrap.h"
#include "mtab.h"
#include "util.h"

//we cant set the value in an h file, so set the value here
const gint SMBSCAN = 0;
const gint WINSSCAN = 1;
const gint IPSCAN = 2;


/* Session management */
static gint save_state (GnomeClient        *client,
			gint			phase,
			GnomeRestartStyle	save_style,
			gint			shutdown,
			GnomeInteractStyle	interact_style,
			gint			fast,
			gpointer		client_data)
{
	gchar *session_id;
	gchar *argv[3];
	gint x, y, w, h;
	
	session_id= gnome_client_get_id (client);
	

	gdk_window_get_geometry (window->window, &x, &y, &w, &h, NULL);
	gnome_config_push_prefix (gnome_client_get_config_prefix (client));
	gnome_config_set_int ("Geometry/x", x);
	gnome_config_set_int ("Geometry/y", y);
	gnome_config_set_int ("Geometry/w", w);
	gnome_config_set_int ("Geometry/h", h);

	gnome_config_pop_prefix ();
	gnome_config_sync();

	argv[0] = (char*) client_data;
	argv[1] = "--discard-session";
	argv[2] = gnome_client_get_config_prefix (client);
	gnome_client_set_discard_command (client, 3, argv);
	gnome_client_set_clone_command (client, 1, argv); 
	gnome_client_set_restart_command (client, 1, argv);

	return TRUE;
}

static gint die (GnomeClient *client, gpointer client_data)
{
	gtk_exit (0);
	return FALSE;
}

/*our general clean up and exit routine*/
void delete_event (GtkWidget *widget, gpointer *data) {
	gtk_exit(0);
}

//someday we might have an option or two
static void parse_an_arg (poptContext state,enum poptCallbackReason reason, const struct poptOption *opt, const char *arg, void *data) { }
static struct poptOption cb_options [] = {
	{ NULL, '\0', POPT_ARG_CALLBACK, parse_an_arg, 0},
	
	{ NULL, '\0', 0, NULL, 0}
};

void prepare_app() {

	GtkWidget *frame;
	GtkWidget *fBottom;
	
	window = gnome_app_new ("gnomba", _("gnomba") );

	gtk_widget_realize (window);
	gtk_signal_connect (GTK_OBJECT (window), "delete_event",
											GTK_SIGNAL_FUNC (delete_event), NULL);

	/*Allow ourselves to be grown and shrunk */
	gtk_window_set_policy( GTK_WINDOW (window),TRUE,TRUE,TRUE);

	/*get this info from the config*/
	//ok so the defaults here should be DEFINES or something;
	gnome_config_push_prefix ("gnomba/");
	gtk_window_set_default_size(GTK_WINDOW (window), 400, 400);
	gtk_widget_set_uposition (window, 20, 20);
	AutoScan = gnome_config_get_int ("Options/AutoScan=0");
	ScanType = gnome_config_get_int ("Options/ScanType=0");
	debug = gnome_config_get_int ("Options/DebugOn=0");
	HideIPC = gnome_config_get_int ("Options/HideIPCShares=1");
	HideDollars = gnome_config_get_int ("Options/HideDollarShares=0");
	OldSamba = gnome_config_get_int ("Options/OldSamba=0");
	NewSamba = gnome_config_get_int ("Options/NewSamba=0");
	NoAuthentication = gnome_config_get_int ("Options/NoAuthentication=0");
	BrokenLookup=	gnome_config_get_int ("Options/BrokenLookup=0");
	DefaultUser = gnome_config_get_string("Options/DefaultUser=guest"); 
	DefaultWorkgroup = gnome_config_get_string("Options/DefaultWorkgroup=");
	SlowScan = gnome_config_get_int("Options/SlowScan=0");
	AutoMount = gnome_config_get_int("Options/AutoMount=1");
	AutoCmd = gnome_config_get_int("Options/AutoCmd=1");
	AutoUnmount = gnome_config_get_int("Options/AutoUnmount=0");
	DefaultMount = gnome_config_get_string("Options/DefaultMount=/tmp");
	DefaultCommand = gnome_config_get_string("Options/DefaultCommand=gmc");
	SilentlyCreateMountPoint = gnome_config_get_int ("Options/SilentlyCreateMountPoint=0");
	DontRemoveCreatedMount = gnome_config_get_int ("Options/DontRemoveCreatedMount=0");
	NetFont = gnome_config_get_string("Options/NetFont=-adobe-helvetica-medium-r-normal-*-*-120-*-*-p-*-iso8859-1"); 
	CommentFont = gnome_config_get_string("Options/CommentFont=-adobe-helvetica-medium-r-normal-*-*-120-*-*-p-*-iso8859-1"); 
	MountFont = gnome_config_get_string("Options/MountFont=-adobe-helvetica-medium-r-normal-*-*-120-*-*-p-*-iso8859-1"); 
	ScanAll = gnome_config_get_int ("Options/ScanAll=0");
	gnome_config_pop_prefix();

	/*create the containers*/
	frame = gtk_vbox_new(FALSE, 0);
	fBottom = gtk_hbox_new(FALSE, 0);
	
	gnome_app_set_contents ( GNOME_APP (window), frame);
	gtk_box_pack_start(GTK_BOX(frame), fBottom, TRUE, TRUE, 0);

	/*setup our parts*/	
	createStatusbar(window);
	createMenu(window);
	createBrowser(fBottom);
	
	/*show it all*/
	gtk_widget_show(fBottom);
	gtk_widget_show(frame);
	gtk_widget_show(window);
}

/**********************MAIN********************************/

/*Create the widgets and go*/
int main (int argc, char *argv[]) {
	
	GnomeClient *client;
	poptContext ctx;

	/* Initialize the i18n stuff */
	bindtextdomain (PACKAGE, GNOMELOCALEDIR);
	textdomain (PACKAGE);

	/* Init Stuff that will be updated by opts */
	gnome_init_with_popt_table("gnomba", VERSION, argc, argv, cb_options, 0, &ctx);
	client= gnome_master_client();

	/* Arrange to be told when something interesting happens.	*/
	gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
											GTK_SIGNAL_FUNC (save_state), (gpointer) argv[0]);
	gtk_signal_connect (GTK_OBJECT (client), "die",
											GTK_SIGNAL_FUNC (die), NULL);
	
	/*clone stuff, still not sure how all this works */
	if (GNOME_CLIENT_CONNECTED (client)) { }
 
	prepare_app();

	if (AutoScan) { 
		setBusyCursor();
		DoScan();
		setNormalCursor();
	}
	gtk_main();
	return 0;
}

/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
