/* Gnomba
 * Copyright (C) 1999 Gnomba Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>

#include "properties.h"
#include "error.h"

static GtkWidget *propbox = NULL;
gint TAutoScan;
gint TDebugOn;
gint THideIPC;
gint THideDollars;
gint TOldSamba;
gint TNewSamba;
gint TNoAuthentication;
gchar *TDefaultUser;
gchar *TDefaultWorkgroup;
gint TBrokenLookup;
gint TSlowScan;
gint TAutoMount;
gint TAutoCmd;
gint TAutoUnmount;
gchar *TDefaultMount;
gchar *TDefaultCommand;
gint TSilentlyCreateMountPoint;
gint TDontRemoveCreatedMount;
gchar *TNetFont;
gchar *TCommentFont;
gchar *TMountFont;
gint TScanType;

struct _EntryData {
  GtkWidget *Modify;
  GtkWidget *Next;
};

typedef struct _EntryData EntryData;

EntryData *ed1, *ed2, *ed3, *ed4, *ed7, *ed8;

GtkWidget *StartIP1;
GtkWidget *StartIP2;
GtkWidget *StartIP3;
GtkWidget *StartIP4;
GtkWidget *rangelist;
GtkWidget *EndIP1;
GtkWidget *EndIP2;
GtkWidget *EndIP3;
GtkWidget *EndIP4;

GtkWidget *frameScan;
GtkWidget *frameWINS;


static void set_scan_type_cb(GtkWidget * widget, gpointer data) {
	
	TScanType = (int) data;
  
	gtk_widget_set_sensitive(frameScan, FALSE);
	gtk_widget_set_sensitive(frameWINS, FALSE);

	if (TScanType == WINSSCAN) 
		gtk_widget_set_sensitive(frameWINS, TRUE);
	else if (TScanType == IPSCAN)	
		gtk_widget_set_sensitive(frameScan, TRUE);
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}



static void entry_property_cb(GtkWidget * widget, gpointer data) {
  gchar **value = (gchar **) data;

  g_return_if_fail(value && GTK_IS_ENTRY(widget));

  *value = g_strdup(gtk_entry_get_text(GTK_ENTRY(widget)));
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}

/*
 *  * Generic toggle button callback
 *   */

static void toggle_button_property_cb(GtkWidget * widget, gpointer data)
{
  gint *value = (gint *) data;

  g_return_if_fail(value && GTK_IS_TOGGLE_BUTTON(widget));

  *value = GTK_TOGGLE_BUTTON(widget)->active;
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}


/*
 * Font selection callbacks
 */

static void font_selection_dialog_button_cb(GtkWidget * widget, gpointer data)
{
  gchar **value = (gchar **) data;
  GtkWidget *fontsel = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "fontsel");
  GtkWidget *entry = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(fontsel), "txtfont");

  g_return_if_fail(GTK_IS_FONT_SELECTION_DIALOG(fontsel) && GTK_IS_ENTRY(entry));

  gtk_widget_hide(fontsel);
  if (value) {
    gtk_entry_set_text(GTK_ENTRY(entry),
		       gtk_font_selection_dialog_get_font_name(GTK_FONT_SELECTION_DIALOG(fontsel)));

    *value = g_strdup(gtk_entry_get_text(GTK_ENTRY(entry)));
    gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
  }
  gtk_widget_destroy(fontsel);
}


static void font_select_cb(GtkWidget * widget, gpointer data)
{
  GtkWidget *txtfont = (GtkWidget *) gtk_object_get_data(GTK_OBJECT(widget), "txtfont");
  GtkWidget *fontsel = gtk_font_selection_dialog_new(_("Font selection ..."));
  GtkFontSelectionDialog *fontselptr = GTK_FONT_SELECTION_DIALOG(fontsel);

  g_return_if_fail(data && GTK_IS_BUTTON(widget) && GTK_IS_ENTRY(txtfont));

  gtk_font_selection_dialog_set_font_name(fontselptr, gtk_entry_get_text(GTK_ENTRY(txtfont)));
  gtk_object_set_data(GTK_OBJECT(fontsel), "txtfont", (gpointer) txtfont);
  gtk_window_set_modal(GTK_WINDOW(fontsel), TRUE);
  gtk_signal_connect(GTK_OBJECT(fontselptr->ok_button), "clicked",
		     GTK_SIGNAL_FUNC(font_selection_dialog_button_cb),
		     data);
  gtk_signal_connect(GTK_OBJECT(fontselptr->cancel_button), "clicked",
		     GTK_SIGNAL_FUNC(font_selection_dialog_button_cb),
		     NULL);
  gtk_object_set_data(GTK_OBJECT(fontselptr->cancel_button), "fontsel", (gpointer) fontsel);
  gtk_object_set_data(GTK_OBJECT(fontselptr->ok_button), "fontsel", (gpointer) fontsel);
  gtk_widget_show(fontsel);
}


static void automount_cb(GtkWidget * widget, gpointer data)
{
  TAutoMount = GTK_TOGGLE_BUTTON(widget)->active;
  gtk_widget_set_sensitive(GTK_WIDGET(data), TAutoMount);
  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));
}


/*
 * Callback called when apply button is pressed.
 * Copy temporary values to actual values
 */

/* Callback recursively called for each node to apply style properties */
void apply_style_properties(GtkCTree * tree, GtkCTreeNode * node, gpointer data)
{
  GtkStyle *nodeStyle;

  /* set net font property */
  if (strcmp(TNetFont, NetFont)) {
    nodeStyle = gtk_style_copy(gtk_widget_get_style(GTK_WIDGET(tree)));
    nodeStyle->font = gdk_font_load(NetFont);
    gtk_ctree_node_set_cell_style(GTK_CTREE(tree), GTK_CTREE_NODE(node), 0, nodeStyle);
  }
  /* set comment font property */
  if (strcmp(TCommentFont, CommentFont)) {
    nodeStyle = gtk_style_copy(gtk_widget_get_style(GTK_WIDGET(tree)));
    nodeStyle->font = gdk_font_load(CommentFont);
    gtk_ctree_node_set_cell_style(GTK_CTREE(tree), GTK_CTREE_NODE(node), 1, nodeStyle);
  }
  /* set mount font property */
  if (strcmp(TMountFont, MountFont)) {
    nodeStyle = gtk_style_copy(gtk_widget_get_style(GTK_WIDGET(tree)));
    nodeStyle->font = gdk_font_load(MountFont);
    gtk_ctree_node_set_cell_style(GTK_CTREE(tree), GTK_CTREE_NODE(node), 2, nodeStyle);
	}
}


static void apply_cb(GtkWidget * w, gint pagenum, gpointer data)
{

  gint row;
  gchar *text;
  gchar key[15];		/*This limits us to 9999 scan ranges */

  if ((pagenum == 0) || (pagenum == -1)) {

    /*we are gonna want to write out the ranges to the file */
		ScanType = TScanType;
    gnome_config_push_prefix("gnomba/");
    gnome_config_set_int("Options/ScanType", ScanType);
    
		gnome_config_set_int("Scan/Max", GTK_CLIST(rangelist)->rows);

    for (row = 0; row < GTK_CLIST(rangelist)->rows; row++) {
      gtk_clist_get_text(GTK_CLIST(rangelist), row, 0, &text);
      sprintf(key, "Scan/Range%d", row);
      gnome_config_set_string(key, text);
    }

    gnome_config_sync();
    gnome_config_pop_prefix();

  }
  if ((pagenum == 1) || (pagenum == -1)) {

    AutoScan = TAutoScan;
    HideIPC = THideIPC;
    HideDollars = THideDollars;
    SilentlyCreateMountPoint = TSilentlyCreateMountPoint;
    DontRemoveCreatedMount = TDontRemoveCreatedMount;

    gnome_config_push_prefix("gnomba/");
    gnome_config_set_int("Options/AutoScan", AutoScan);
    gnome_config_set_int("Options/HideIPCShares", HideIPC);
    gnome_config_set_int("Options/HideDollarShares", HideDollars);
    gnome_config_set_int("Options/SilentlyCreateMountPoint", SilentlyCreateMountPoint);
    gnome_config_set_int("Options/DontRemoveCreatedMount", DontRemoveCreatedMount);

    gnome_config_sync();
    gnome_config_pop_prefix();

  }
  if ((pagenum == 2) || (pagenum == -1)) {

    NetFont = TNetFont;
    CommentFont = TCommentFont;
    MountFont = TMountFont;
    gnome_config_push_prefix("gnomba/");

    gnome_config_set_string("Options/NetFont", NetFont);
    gnome_config_set_string("Options/CommentFont", CommentFont);
    gnome_config_set_string("Options/MountFont", MountFont);
    gnome_config_sync();
    gnome_config_pop_prefix();

  }
  if ((pagenum == 3) || (pagenum == -1)) {

    NoAuthentication = TNoAuthentication;
    DefaultUser = TDefaultUser;
    DefaultWorkgroup = TDefaultWorkgroup;

    gnome_config_push_prefix("gnomba/");
    gnome_config_set_int("Options/NoAuthentication", NoAuthentication);
    gnome_config_set_string("Options/DefaultUser", DefaultUser);
    gnome_config_set_string("Options/DefaultWorkgroup", DefaultWorkgroup);
    gnome_config_sync();
    gnome_config_pop_prefix();

  }
  if ((pagenum == 3) || (pagenum == -1)) {

		if ((TAutoCmd)&&(!strcmp(TDefaultCommand,""))) {
			ShowError("If it is set to autorun a command then a command must be specified");
			return;
		}
					
					
    AutoMount = TAutoMount;
    AutoCmd = TAutoCmd;
    AutoUnmount = TAutoUnmount;
    DefaultMount = TDefaultMount;
    DefaultCommand = TDefaultCommand;

    gnome_config_push_prefix("gnomba/");
    gnome_config_set_int("Options/AutoMount", AutoMount);
    gnome_config_set_int("Options/AutoUnmount", AutoUnmount);
    gnome_config_set_int("Options/AutoCmd", AutoCmd);
    gnome_config_set_string("Options/DefaultMount", DefaultMount);
    gnome_config_set_string("Options/DefaultCommand", DefaultCommand);
    gnome_config_sync();
    gnome_config_pop_prefix();
  }
  if ((pagenum == 4) || (pagenum == -1)) {
    debug = TDebugOn;
    OldSamba = TOldSamba;
    NewSamba = TNewSamba;
    BrokenLookup = TBrokenLookup;
    SlowScan = TSlowScan;

    gnome_config_push_prefix("gnomba/");
    gnome_config_set_int("Options/DebugOn", debug);
    gnome_config_set_int("Options/OldSamba", OldSamba);
    gnome_config_set_int("Options/NewSamba", NewSamba);
    gnome_config_set_int("Options/BrokenLookup", BrokenLookup);
    gnome_config_set_int("Options/SlowScan", SlowScan);
    gnome_config_sync();
    gnome_config_pop_prefix();
  }
}

static void destroy_cb(GtkWidget * w, gpointer data)
{

  g_free(ed1);
  g_free(ed2);
  g_free(ed3);
  g_free(ed4);
  g_free(ed7);
  g_free(ed8);

  gtk_widget_destroy(propbox);
  propbox = NULL;
}

/*if Modify is set, this updates the equivalent box
   if Next is set, moves focus */
static void updateEntry_cb(GtkWidget * w, gpointer * edtmp)
{
  gchar *gtmp;
  EntryData *ed;
  gchar *dot;

  gnome_property_box_changed(GNOME_PROPERTY_BOX(propbox));

  ed = (EntryData *) edtmp;
  gtmp = gtk_entry_get_text(GTK_ENTRY(w));

  if ((dot = strchr(gtmp, '.')) != NULL) {
    *dot = '\0';
    gtk_entry_set_text(GTK_ENTRY(w), gtmp);
    gtk_widget_grab_focus(ed->Next);
  }
  if (ed->Modify != NULL)
    gtk_entry_set_text(GTK_ENTRY(ed->Modify), gtmp);

  if ((strlen(gtmp) == 3) && (ed->Next != NULL))
    gtk_widget_grab_focus(ed->Next);
}

static void AddRange_cb(GtkWidget * w, gpointer data)
{

  gchar *text[1];

  text[0] = g_strdup_printf("%d.%d.%d.%d - %d.%d.%d.%d",
	  atoi(gtk_entry_get_text(GTK_ENTRY(StartIP1))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(StartIP2))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(StartIP3))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(StartIP4))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(EndIP1))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(EndIP2))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(EndIP3))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(EndIP4))));
  gtk_clist_append(GTK_CLIST(rangelist), text);
  g_free(text[0]);
}


/* If we come here, then the user has selected a row in the list. */
void selection_made(GtkWidget * clist, gint row, gint column,
		    GdkEventButton * event, gpointer data)
{
  gchar *text;
  gchar s1[4], s2[4], s3[4], s4[4], e1[4], e2[4], e3[4], e4[4];


  gtk_clist_get_text(GTK_CLIST(clist), row, column, &text);
  sscanf(text, "%[0-9].%[0-9].%[0-9].%[0-9] - %[0-9].%[0-9].%[0-9].%[0-9]", s1, s2, s3, s4, e1, e2, e3, e4);

  gtk_entry_set_text(GTK_ENTRY(StartIP1), s1);
  gtk_entry_set_text(GTK_ENTRY(StartIP2), s2);
  gtk_entry_set_text(GTK_ENTRY(StartIP3), s3);
  gtk_entry_set_text(GTK_ENTRY(StartIP4), s4);
  gtk_entry_set_text(GTK_ENTRY(EndIP1), e1);
  gtk_entry_set_text(GTK_ENTRY(EndIP2), e2);
  gtk_entry_set_text(GTK_ENTRY(EndIP3), e3);
  gtk_entry_set_text(GTK_ENTRY(EndIP4), e4);

  return;
}

static void RemoveRange_cb(GtkWidget * w, gpointer data)
{

  GList *selection;

  selection = GTK_CLIST(rangelist)->selection;
  if (selection == NULL)
    return;

  gtk_clist_remove(GTK_CLIST(rangelist), (gint) selection->data);
}

static void UpdateRange_cb(GtkWidget * w, gpointer data)
{
  GList *selection;
  gchar *text;

  selection = GTK_CLIST(rangelist)->selection;
  if (selection == NULL)
    return;

  text = g_strdup_printf("%d.%d.%d.%d - %d.%d.%d.%d",
	  atoi(gtk_entry_get_text(GTK_ENTRY(StartIP1))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(StartIP2))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(StartIP3))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(StartIP4))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(EndIP1))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(EndIP2))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(EndIP3))),
	  atoi(gtk_entry_get_text(GTK_ENTRY(EndIP4))));
  gtk_clist_set_text(GTK_CLIST(rangelist), (gint) selection->data, 0, text);
  g_free(text);
}

void show_properties_dialog()
{
  GtkWidget *cpage;
  GtkWidget *hbox1, *hbox2, *vbox1, *fbox;
  GtkWidget *label1;
  GtkWidget *label;
  gchar *listTitles[1];
  GtkWidget *rangeframe;
  GtkWidget *button;
  GtkWidget *chkbox;
  GtkWidget *usertf;
  GtkWidget *wrkgrptf;
  GtkWidget *txtcmd;
  GtkWidget *txtrt;
  GtkWidget *txtfont;
  GtkWidget *butfont;
  GtkWidget *radio;
	GSList *rgroup;
	
  /*for loading ranges */
  gint row;
  gint max;
  gchar *text[1];
  gchar key[15];


  if (propbox)
    return;


  /* create properties screen */
  propbox = gnome_property_box_new();
  gnome_dialog_set_parent(GNOME_DIALOG(propbox),GTK_WINDOW(window));
  gtk_window_set_modal(GTK_WINDOW(propbox), TRUE);
  gtk_window_set_title(GTK_WINDOW(&GNOME_PROPERTY_BOX(propbox)->dialog.window), _("Gnomba Preferences"));

  cpage = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_SMALL);

  TScanType = ScanType;
  
	/*The First page, all our scanning stuff*/
	vbox1 = gtk_vbox_new(FALSE, 0);
  gtk_widget_show(vbox1);
  gtk_container_add(GTK_CONTAINER(cpage), vbox1);

	/*set these up here so they can be set inactive by the radiobutton*/
	frameScan = gtk_frame_new(_("IP Scan options"));
	frameWINS= gtk_frame_new(_("WINS Server options"));
		
	
  /*** Scan using Samba's Master Browser concept***/
	radio = gtk_radio_button_new_with_label(NULL, _("Scan using SMB protocol")); 
	rgroup = gtk_radio_button_group(GTK_RADIO_BUTTON (radio));
	gtk_signal_connect(GTK_OBJECT(radio), "clicked",
            (GtkSignalFunc) set_scan_type_cb, 0);
  gtk_box_pack_start(GTK_BOX(vbox1), radio, TRUE, TRUE, GNOME_PAD_SMALL);
	gtk_widget_show(radio);
	if (TScanType == SMBSCAN) gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), TRUE);
 

	/*** Scan using a WINS server ***/
 	radio = gtk_radio_button_new_with_label(rgroup, _("Scan using WINS protocol (not yet implemented)")); 
	gtk_signal_connect(GTK_OBJECT(radio), "clicked",
            (GtkSignalFunc) set_scan_type_cb, GINT_TO_POINTER(1));
  gtk_box_pack_start(GTK_BOX(vbox1), radio, TRUE, TRUE, GNOME_PAD_SMALL);
	gtk_widget_show(radio);
	if (TScanType == WINSSCAN) gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), TRUE);
	else gtk_widget_set_sensitive(frameWINS, FALSE);

  gtk_box_pack_start(GTK_BOX(vbox1), frameWINS, FALSE, FALSE, 5);
 
	
	/*** Scan using our ip scanning method ***/
 	radio = gtk_radio_button_new_with_label(gtk_radio_button_group(GTK_RADIO_BUTTON(radio)), _("IP Scan method")); 
	gtk_signal_connect(GTK_OBJECT(radio), "clicked",
            (GtkSignalFunc) set_scan_type_cb, GINT_TO_POINTER(2));
  gtk_box_pack_start(GTK_BOX(vbox1), radio, TRUE, TRUE, GNOME_PAD_SMALL);
	gtk_widget_show(radio);
	if (TScanType == IPSCAN) gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(radio), TRUE);
	else gtk_widget_set_sensitive(frameScan, FALSE);
	

	/*List of ip ranges */
	gtk_box_pack_start(GTK_BOX(vbox1), frameScan, FALSE, FALSE, 5);
	hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_widget_show(hbox1);
  gtk_container_add(GTK_CONTAINER(frameScan), hbox1);
  chkbox = gtk_check_button_new_with_label(_("Scan using SMB protocol"));
  
	listTitles[0] = g_strdup(_("Scan Range"));
  rangelist = gtk_clist_new_with_titles(1, listTitles);
  gtk_clist_set_column_width(GTK_CLIST(rangelist), 0, 45);
  gtk_clist_set_selection_mode(GTK_CLIST(rangelist), GTK_SELECTION_SINGLE);
  rangeframe = gtk_scrolled_window_new(NULL, NULL);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(rangeframe), GTK_POLICY_AUTOMATIC, GTK_POLICY_ALWAYS);
  gtk_container_add(GTK_CONTAINER(rangeframe), rangelist);
  gtk_box_pack_start(GTK_BOX(hbox1), rangeframe, TRUE, TRUE, 0);
  
	gtk_widget_show(rangeframe);
  gtk_widget_show(rangelist);

  gtk_signal_connect(GTK_OBJECT(rangelist), "select_row",
		     GTK_SIGNAL_FUNC(selection_made),
		     NULL);


  vbox1 = gtk_vbox_new(FALSE, 6);
  gtk_widget_show(vbox1);
  gtk_box_pack_start(GTK_BOX(hbox1), vbox1, FALSE, TRUE, 5);

  /*to and from labels and entries */
  label1 = gtk_label_new(_("From:"));
  gtk_box_pack_start(GTK_BOX(vbox1), label1, FALSE, TRUE, 0);
  gtk_misc_set_alignment(GTK_MISC(label1), 0, 0);
  gtk_widget_show(label1);

  hbox2 = gtk_hbox_new(FALSE, 0);
  gtk_widget_show(hbox2);
  gtk_box_pack_start(GTK_BOX(vbox1), hbox2, TRUE, TRUE, 0);

  StartIP1 = gtk_entry_new_with_max_length(3);
  gtk_widget_set_usize(StartIP1, 30, 20);
  gtk_widget_show(StartIP1);
  gtk_box_pack_start(GTK_BOX(hbox2), StartIP1, FALSE, FALSE, 0);

  StartIP2 = gtk_entry_new_with_max_length(3);
  gtk_widget_set_usize(StartIP2, 30, 20);
  gtk_widget_show(StartIP2);
  gtk_box_pack_start(GTK_BOX(hbox2), StartIP2, FALSE, FALSE, 0);

  StartIP3 = gtk_entry_new_with_max_length(3);
  gtk_widget_set_usize(StartIP3, 30, 20);
  gtk_widget_show(StartIP3);
  gtk_box_pack_start(GTK_BOX(hbox2), StartIP3, FALSE, FALSE, 0);

  StartIP4 = gtk_entry_new_with_max_length(3);
  gtk_widget_set_usize(StartIP4, 30, 20);
  gtk_widget_show(StartIP4);
  gtk_box_pack_start(GTK_BOX(hbox2), StartIP4, FALSE, FALSE, 0);

  label = gtk_label_new(_("To:"));
  gtk_widget_show(label);
  gtk_box_pack_start(GTK_BOX(vbox1), label, FALSE, TRUE, 0);
  gtk_misc_set_alignment(GTK_MISC(label), 0, 0);

  hbox2 = gtk_hbox_new(FALSE, 0);
  gtk_widget_show(hbox2);
  gtk_box_pack_start(GTK_BOX(vbox1), hbox2, TRUE, TRUE, 0);

  EndIP1 = gtk_entry_new_with_max_length(3);
  gtk_widget_set_usize(EndIP1, 30, 20);
  gtk_widget_show(EndIP1);
  gtk_box_pack_start(GTK_BOX(hbox2), EndIP1, FALSE, FALSE, 0);
  gtk_widget_set_sensitive(EndIP1, FALSE);

  EndIP2 = gtk_entry_new_with_max_length(3);
  gtk_widget_set_usize(EndIP2, 30, 20);
  gtk_widget_show(EndIP2);
  gtk_box_pack_start(GTK_BOX(hbox2), EndIP2, FALSE, FALSE, 0);
  gtk_widget_set_sensitive(EndIP2, FALSE);

  EndIP3 = gtk_entry_new_with_max_length(3);
  gtk_widget_set_usize(EndIP3, 30, 20);
  gtk_widget_show(EndIP3);
  gtk_box_pack_start(GTK_BOX(hbox2), EndIP3, FALSE, FALSE, 0);

  EndIP4 = gtk_entry_new_with_max_length(3);
  gtk_widget_set_usize(EndIP4, 30, 20);
  gtk_widget_show(EndIP4);
  gtk_box_pack_start(GTK_BOX(hbox2), EndIP4, FALSE, FALSE, 0);

  /*connect to changed signal */

  ed1 = g_new(EntryData, 1);
  ed1->Modify = EndIP1;
  ed1->Next = StartIP2;
  gtk_signal_connect(GTK_OBJECT(StartIP1), "changed",
		     (GtkSignalFunc) updateEntry_cb, (gpointer) ed1);

  ed2 = g_new(EntryData, 1);
  ed2->Modify = EndIP2;
  ed2->Next = StartIP3;
  gtk_signal_connect(GTK_OBJECT(StartIP2), "changed",
		     (GtkSignalFunc) updateEntry_cb, (gpointer) ed2);

  ed3 = g_new(EntryData, 1);
  ed3->Modify = EndIP3;
  ed3->Next = StartIP4;
  gtk_signal_connect(GTK_OBJECT(StartIP3), "changed",
		     (GtkSignalFunc) updateEntry_cb, (gpointer) ed3);

  ed4 = g_new(EntryData, 1);
  ed4->Modify = NULL;
  ed4->Next = EndIP3;
  gtk_signal_connect(GTK_OBJECT(StartIP4), "changed",
		     (GtkSignalFunc) updateEntry_cb, (gpointer) ed4);

  ed7 = g_new(EntryData, 1);
  ed7->Modify = NULL;
  ed7->Next = EndIP4;
  gtk_signal_connect(GTK_OBJECT(EndIP3), "changed",
		     (GtkSignalFunc) updateEntry_cb, (gpointer) ed7);

  ed8 = g_new(EntryData, 1);
  ed8->Modify = NULL;
  ed8->Next = NULL;
  gtk_signal_connect(GTK_OBJECT(EndIP4), "changed",
		     (GtkSignalFunc) updateEntry_cb, (gpointer) ed8);


  /*add, remove, update buttons */
  hbox2 = gtk_hbox_new(TRUE, 0);
  gtk_widget_show(hbox2);
  gtk_box_pack_start(GTK_BOX(vbox1), hbox2, FALSE, FALSE, 5);

  button = gtk_button_new_with_label(_("Add"));
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox2), button, FALSE, TRUE, 5);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
		     (GtkSignalFunc) AddRange_cb, NULL);

  button = gtk_button_new_with_label(_("Remove"));
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox2), button, FALSE, TRUE, 5);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
		     (GtkSignalFunc) RemoveRange_cb, NULL);

  button = gtk_button_new_with_label(_("Update"));
  gtk_widget_show(button);
  gtk_box_pack_start(GTK_BOX(hbox2), button, FALSE, TRUE, 5);
  gtk_signal_connect(GTK_OBJECT(button), "clicked",
		     (GtkSignalFunc) UpdateRange_cb, NULL);

  gtk_widget_show(frameWINS);
  gtk_widget_show(frameScan);
  
	gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_SMALL);
  gtk_widget_show(cpage);
  label = gtk_label_new(_("Scan Method"));
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(propbox), cpage, label); 

/********** Other Options **************/

  cpage = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_SMALL);

  TAutoScan = AutoScan;
  TDebugOn = debug;
  THideIPC = HideIPC;
  THideDollars = HideDollars;
  TOldSamba = OldSamba;
  TNewSamba = NewSamba;
  TNoAuthentication = NoAuthentication;
  TBrokenLookup = BrokenLookup;
  TSlowScan = SlowScan;
  TAutoMount = AutoMount;
  TAutoCmd = AutoCmd;
  TAutoUnmount = AutoUnmount;
  TDefaultCommand = DefaultCommand;
  TDefaultMount = DefaultMount;
  TSilentlyCreateMountPoint = SilentlyCreateMountPoint;
  TDontRemoveCreatedMount = DontRemoveCreatedMount;

  /* AutoScan on startup */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Scan on program open"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TAutoScan;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
	    (GtkSignalFunc) toggle_button_property_cb, (gpointer) & TAutoScan);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);




  /*HideIPC */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Hide IPC$ shares"));
  GTK_TOGGLE_BUTTON(chkbox)->active = THideIPC;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
	     (GtkSignalFunc) toggle_button_property_cb, (gpointer) & THideIPC);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);

  /*Hide all $ */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Hide all $ shares"));
  GTK_TOGGLE_BUTTON(chkbox)->active = THideDollars;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
	  (GtkSignalFunc) toggle_button_property_cb, (gpointer) & THideDollars);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);


/*Silently create non existant mount points */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Silently create non existant mount point directories?"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TSilentlyCreateMountPoint;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc) toggle_button_property_cb, (gpointer) & TSilentlyCreateMountPoint);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);


/*Do not remove created mount point directories */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Don't remove created mount point directories?"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TDontRemoveCreatedMount;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc) toggle_button_property_cb, (gpointer) & TDontRemoveCreatedMount);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);

  gtk_widget_show(cpage);
  label = gtk_label_new(_("Options"));
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(propbox), cpage, label);

/****** Font Selection ******/

  TNetFont = NetFont;
  TCommentFont = CommentFont;
  TMountFont = MountFont;

  cpage = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_SMALL);

  /* Net font */
  hbox1 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, FALSE, GNOME_PAD_SMALL);
  label = gtk_label_new(_("Net Font"));
  //gtk_widget_set_usize(label, 120, 14);
  gtk_box_pack_start(GTK_BOX(hbox1), label, FALSE, FALSE, GNOME_PAD_SMALL);
  txtfont = gtk_entry_new();
  gtk_signal_connect(GTK_OBJECT(txtfont), "changed",
		  (GtkSignalFunc) entry_property_cb, (gpointer) & TNetFont);
  gtk_entry_set_text(GTK_ENTRY(txtfont), TNetFont);
  gtk_box_pack_start(GTK_BOX(hbox1), txtfont, TRUE, TRUE, GNOME_PAD_SMALL);
  butfont = gtk_button_new_with_label(_("Select font ..."));
  gtk_box_pack_start(GTK_BOX(hbox1), butfont, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_object_set_data(GTK_OBJECT(butfont), "txtfont", (gpointer) txtfont);
  gtk_signal_connect(GTK_OBJECT(butfont), "clicked",
		     (GtkSignalFunc) font_select_cb, (gpointer) & TNetFont);
  gtk_widget_show(label);
  gtk_widget_show(txtfont);
  gtk_widget_show(butfont);
  gtk_widget_show(hbox1);

  /* Comment font */
  hbox1 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, FALSE, GNOME_PAD_SMALL);
  label = gtk_label_new(_("Comment Font"));
  //gtk_widget_set_usize(label, 120, 14);
  gtk_box_pack_start(GTK_BOX(hbox1), label, FALSE, FALSE, GNOME_PAD_SMALL);
  txtfont = gtk_entry_new();
  gtk_signal_connect(GTK_OBJECT(txtfont), "changed",
	      (GtkSignalFunc) entry_property_cb, (gpointer) & TCommentFont);
  gtk_entry_set_text(GTK_ENTRY(txtfont), TCommentFont);
  gtk_box_pack_start(GTK_BOX(hbox1), txtfont, TRUE, TRUE, GNOME_PAD_SMALL);
  butfont = gtk_button_new_with_label(_("Select font ..."));
  gtk_box_pack_start(GTK_BOX(hbox1), butfont, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_object_set_data(GTK_OBJECT(butfont), "txtfont", (gpointer) txtfont);
  gtk_signal_connect(GTK_OBJECT(butfont), "clicked",
		 (GtkSignalFunc) font_select_cb, (gpointer) & TCommentFont);
  gtk_widget_show(label);
  gtk_widget_show(txtfont);
  gtk_widget_show(butfont);
  gtk_widget_show(hbox1);

  /* Mount font */
  hbox1 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, FALSE, GNOME_PAD_SMALL);
  label = gtk_label_new(_("Mount Font"));
  //gtk_widget_set_usize(label, 120, 14);
  gtk_box_pack_start(GTK_BOX(hbox1), label, FALSE, FALSE, GNOME_PAD_SMALL);
  txtfont = gtk_entry_new();
  gtk_signal_connect(GTK_OBJECT(txtfont), "changed",
		(GtkSignalFunc) entry_property_cb, (gpointer) & TMountFont);
  gtk_entry_set_text(GTK_ENTRY(txtfont), TMountFont);
  gtk_box_pack_start(GTK_BOX(hbox1), txtfont, TRUE, TRUE, GNOME_PAD_SMALL);
  butfont = gtk_button_new_with_label(_("Select font ..."));
  gtk_box_pack_start(GTK_BOX(hbox1), butfont, FALSE, FALSE, GNOME_PAD_SMALL);
  gtk_object_set_data(GTK_OBJECT(butfont), "txtfont", (gpointer) txtfont);
  gtk_signal_connect(GTK_OBJECT(butfont), "clicked",
		   (GtkSignalFunc) font_select_cb, (gpointer) & TMountFont);
  gtk_widget_show(label);
  gtk_widget_show(txtfont);
  gtk_widget_show(butfont);
  gtk_widget_show(hbox1);

  gtk_widget_show(cpage);
  label = gtk_label_new(_("Font Selection"));
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(propbox), cpage, label);

/****** Credentials settings ******/

  cpage = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_SMALL);

  /* Default user */
  hbox1 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, FALSE, GNOME_PAD_SMALL);
  label = gtk_label_new(_("Default User"));
  gtk_box_pack_start(GTK_BOX(hbox1), label, FALSE, FALSE, GNOME_PAD_SMALL);
  usertf = gtk_entry_new();
  gtk_signal_connect(GTK_OBJECT(usertf), "changed",
	      (GtkSignalFunc) entry_property_cb, (gpointer) & TDefaultUser);
  gtk_entry_set_text(GTK_ENTRY(usertf), DefaultUser);
  gtk_box_pack_start(GTK_BOX(hbox1), usertf, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(label);
  gtk_widget_show(usertf);
  gtk_widget_show(hbox1);

  /* Default Workgroup */
  hbox1 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, FALSE, GNOME_PAD_SMALL);
  label = gtk_label_new(_("Default Workgroup"));
  gtk_box_pack_start(GTK_BOX(hbox1), label, FALSE, FALSE, GNOME_PAD_SMALL);
  wrkgrptf = gtk_entry_new();
  gtk_signal_connect(GTK_OBJECT(wrkgrptf), "changed",
	 (GtkSignalFunc) entry_property_cb, (gpointer) & TDefaultWorkgroup);
  gtk_entry_set_text(GTK_ENTRY(wrkgrptf), DefaultWorkgroup);
  gtk_box_pack_start(GTK_BOX(hbox1), wrkgrptf, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(label);
  gtk_widget_show(wrkgrptf);
  gtk_widget_show(hbox1);

/*Do not ask for authentication on/off */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Dont ask for authentication if no guest shares"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TNoAuthentication;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
  (GtkSignalFunc) toggle_button_property_cb, (gpointer) & TNoAuthentication);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);

  gtk_widget_show(cpage);
  label = gtk_label_new(_("Credentials"));
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(propbox), cpage, label);

/****** Command Settings******/

  cpage = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_SMALL);

  /* Default cmd */
  hbox1 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, FALSE, GNOME_PAD_SMALL);
  label = gtk_label_new(_("Default Command:"));
  gtk_label_set_justify(GTK_LABEL(label), GTK_JUSTIFY_LEFT);
  gtk_box_pack_start(GTK_BOX(hbox1), label, FALSE, FALSE, GNOME_PAD_SMALL);

  txtcmd = gtk_entry_new();
  if (TDefaultCommand)
    gtk_entry_set_text(GTK_ENTRY(txtcmd), TDefaultCommand);
  gtk_signal_connect(GTK_OBJECT(txtcmd), "changed",
		     (GtkSignalFunc) entry_property_cb, & TDefaultCommand);
  gtk_box_pack_start(GTK_BOX(hbox1), txtcmd, FALSE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(label);
  gtk_widget_show(txtcmd);
  gtk_widget_show(hbox1);

  /*setup frame */
  fbox = gtk_frame_new(NULL);

  /*automount */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Auto mount on doubleclick"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TAutoMount;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc) automount_cb, fbox);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);

  gtk_box_pack_start(GTK_BOX(cpage), fbox, FALSE, TRUE, GNOME_PAD_SMALL);
  vbox1 = gtk_vbox_new(TRUE, 0);
  gtk_container_add(GTK_CONTAINER(fbox), vbox1);

  /* Default mnt */
  hbox1 = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox1), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  label = gtk_label_new(_("Default root for mount point:"));
  gtk_box_pack_start(GTK_BOX(hbox1), label, FALSE, FALSE, GNOME_PAD_SMALL);
  txtrt = gtk_entry_new();
  if (TDefaultMount)
    gtk_entry_set_text(GTK_ENTRY(txtrt), TDefaultMount);
  gtk_signal_connect(GTK_OBJECT(txtrt), "changed",
		     (GtkSignalFunc) entry_property_cb, &TDefaultMount);
  gtk_box_pack_start(GTK_BOX(hbox1), txtrt, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(label);
  gtk_widget_show(txtrt);
  gtk_widget_show(hbox1);


  /*Run command on mount */
  chkbox = gtk_check_button_new_with_label(_("Run command on automount"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TAutoCmd;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc) toggle_button_property_cb, &TAutoCmd);
  gtk_box_pack_start(GTK_BOX(vbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  
	/*unmount after command */
  /*
	chkbox = gtk_check_button_new_with_label(_("Auto-unmount after command"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TAutoUnmount;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc) autounmount_cb, NULL);
  gtk_box_pack_start(GTK_BOX(vbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  */
  
	gtk_widget_set_sensitive(fbox, TAutoMount);

  gtk_widget_show(vbox1);
  gtk_widget_show(fbox);
  gtk_widget_show(cpage);
  label = gtk_label_new(_("Command Settings"));
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(propbox), cpage, label); 

/********** Advanced Options **************/

  cpage = gtk_vbox_new(FALSE, 0);
  gtk_container_set_border_width(GTK_CONTAINER(cpage), GNOME_PAD_SMALL);

  /*debug on/off */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Write debug info to tty"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TDebugOn;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc) toggle_button_property_cb, &TDebugOn);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);

/*old samba on/off */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Using pre 2.0.5a samba"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TOldSamba;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc) toggle_button_property_cb, &TOldSamba);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);

/*New samba on/off */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Using samba 2.0.6 or greater"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TNewSamba;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc) toggle_button_property_cb, &TNewSamba);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);

/*Stupid borken win95 workaround on/off */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Evil 95 workaround (requires root privleges)"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TBrokenLookup;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc) toggle_button_property_cb, &TBrokenLookup);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);

/*Slow as death option, for those who need it */
  hbox1 = gtk_hbox_new(TRUE, 0);
  gtk_box_pack_start(GTK_BOX(cpage), hbox1, FALSE, TRUE, GNOME_PAD_SMALL);
  chkbox = gtk_check_button_new_with_label(_("Increased Scan Timeouts"));
  GTK_TOGGLE_BUTTON(chkbox)->active = TSlowScan;
  gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
		     (GtkSignalFunc) toggle_button_property_cb, &TSlowScan);
  gtk_box_pack_start(GTK_BOX(hbox1), chkbox, TRUE, TRUE, GNOME_PAD_SMALL);
  gtk_widget_show(chkbox);
  gtk_widget_show(hbox1);

  gtk_widget_show(cpage);
  label = gtk_label_new(_("Advanced Options"));
  gnome_property_box_append_page(GNOME_PROPERTY_BOX(propbox), cpage, label); 

  /* Apply & Destroy connection */

  gtk_signal_connect(GTK_OBJECT(propbox), "destroy",
		     GTK_SIGNAL_FUNC(destroy_cb), NULL);
  gtk_signal_connect(GTK_OBJECT(propbox), "apply",
		     GTK_SIGNAL_FUNC(apply_cb), NULL);
  gtk_widget_show(propbox);

  /*this will probably be done elsewhere, though we will still need to populate rangelist */

  gnome_config_push_prefix("gnomba/");
  max = gnome_config_get_int("Scan/Max=-1");

  for (row = 0; row < max; row++) {
    sprintf(key, "Scan/Range%d", row);
    text[0] = gnome_config_get_string(key);
    gtk_clist_append(GTK_CLIST(rangelist), text);
  }
  gnome_config_pop_prefix();
}

/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
