/* Gnomba
 * Copyright (C) 1999 Gnomba Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *	
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the	
 * GNU General Public License for more details.
 *	
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	
 * 02111-1307, USA.
 */

#ifndef __GNOMBA_H__
#define __GNOMBA_H__

#include "config.h"
#include <gnome.h>

struct _mounted_smbfs {
	gchar * machineName;
	gchar * shareName;
	gchar * mountName;
	struct _mounted_smbfs * next; 
};

enum ShareType_e {
	DISC_SHARE = 0,
	PRINTER_SHARE,
	UNKNOWN_SHARE,
	IPC_SHARE
};


/*this really should be broken into seperate structures*/
struct _machineNode {
	gchar * machineName;
	gchar * workgroupName;
	gchar * mountName;
	gchar * shareName;
	gchar * shareClean;
	gchar * mountClean;
	int shareType;
	gchar * ip;
	struct _machineNode * next;
	gchar * username;
	gchar * passwd;
	GList * machine;
	gint scanned;
	gint mountCreated;
	gint mounted;
};

typedef struct _mounted_smbfs mountedSmbfs;
typedef struct _machineNode machineNode;

mountedSmbfs *rootMounted;
machineNode *currnode;

GtkWidget *workgrouplist;
GtkWidget *workgroupframe;

int os_x, os_y, os_w, os_h;
gint debug;
gint AutoScan;
gint ScanType;
gint HideIPC;
gint HideDollars;
gint OldSamba;
gint NewSamba;
gint NoAuthentication;
gint didFind;
gchar *DefaultUser; 
gchar *DefaultWorkgroup;
gint BrokenLookup;
gint SlowScan;
gint AutoCmd;
gint AutoMount;
gint AutoUnmount;
gchar * DefaultMount;
gchar * DefaultCommand;
gint SilentlyCreateMountPoint; 
gint DontRemoveCreatedMount; 
gint docmd;
gint domnt;
gchar *NetFont; 
gchar *CommentFont; 
gchar *MountFont; 
gint ScanAll;

const gint extern SMBSCAN;
const gint extern WINSSCAN;
const gint extern IPSCAN;
GtkWidget *window;

struct _scanrange {
	char start[4][3];
	char end[4][3];
	struct _scanrange * next;
};

typedef struct _scanrange scanrange;

GdkPixmap *network_pixmap;
GdkPixmap *workgroup_pixmap;
GdkPixmap *machine_pixmap;
GdkPixmap *share_pixmap;
GdkPixmap *printer_pixmap;

GdkBitmap *network_mask;
GdkBitmap *workgroup_mask;
GdkBitmap *machine_mask;
GdkBitmap *share_mask;
GdkBitmap *printer_mask;

gchar * makeClean(gchar *);

#endif /* __GNOMBA_H__ */

/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
