/* Gnomba
 * Copyright (C) 1999 Gnomba Team
 *
 * browser.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *	
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the	
 * GNU General Public License for more details.
 *	
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	
 * 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include <errno.h>

#include "browser.h"
#include "net.h"
#include "error.h"
#include "smbwrap.h"
#include "util.h"
#include "menu.h"
#include <sys/stat.h>

#include "images/workgroup.xpm"
#include "images/machine.xpm"
#include "images/network.xpm"
#include "images/share.xpm"
#include "images/printer.xpm"

/* structure passed around when using pop-up password screens */
typedef struct _PWInfo {
	machineNode *info;
	GtkWidget *username;
	GtkWidget *passwd;
	GtkWidget *mount;
	GtkWidget *workgroup;
} PWInfo;


/******************** FIND DIALOG ***************************/

/*close the gnome dialog passed to us.*/
void closePrompt(GnomeDialog * widget, int button, gpointer * gp) {
	gnome_dialog_close(widget);
}

/*compare function used for find*/
gint findcompare(gconstpointer a, gconstpointer b) {
	machineNode *mn;
	gchar *lbl;

	if ((a == NULL) || (b == NULL))
		return -1;
	mn = (machineNode *) a;
	lbl = (gchar *) b;

	if (mn->workgroupName != NULL) {
		if (strcmp(mn->workgroupName, lbl) == 0)
			return 0;
	}
	if (mn->machineName != NULL) {
		if (strcmp(mn->machineName, lbl) == 0)
			return 0;
	}
	if (mn->shareName != NULL) {
		if (strcmp(mn->shareName, lbl) == 0)
			return 0;
	}
	return (-1);
}

/*We got a string to look for, call find*/
void find_cb(GnomeDialog * widget, int button, gpointer data) {
	if (button == 0) {
			GtkCTreeNode *tmpn;

			tmpn = gtk_ctree_find_by_row_data_custom(GTK_CTREE(workgrouplist), NULL,
				gtk_entry_get_text(GTK_ENTRY(data)), findcompare);

			gtk_ctree_select(GTK_CTREE(workgrouplist), tmpn);
		}
}

/*create the Find dialog and show it*/
void doFind() {
	GtkWidget *windowPrompt;
	GtkWidget *label;
	GtkWidget *txtFind;
	GtkWidget *vbox;
	GtkWidget *hbox;

	windowPrompt = gnome_dialog_new(_("Find Dialog"),
		GNOME_STOCK_BUTTON_OK,GNOME_STOCK_BUTTON_CANCEL,NULL);
	gnome_dialog_set_parent(GNOME_DIALOG(windowPrompt),GTK_WINDOW(window));

	vbox = GNOME_DIALOG(windowPrompt)->vbox;

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, GNOME_PAD_SMALL);

	label = gtk_label_new(_("Find:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);

	txtFind = gtk_entry_new();
	gtk_box_pack_start(GTK_BOX(hbox), txtFind, FALSE, FALSE, GNOME_PAD_SMALL);
	gnome_dialog_editable_enters(GNOME_DIALOG(windowPrompt), GTK_EDITABLE(txtFind));
	gtk_widget_show(hbox);
	gtk_widget_show(label);
	gtk_widget_show(txtFind);

	gtk_signal_connect(GTK_OBJECT(windowPrompt), "clicked",
										 GTK_SIGNAL_FUNC(find_cb), txtFind);
	gtk_signal_connect(GTK_OBJECT(windowPrompt), "clicked",
										 GTK_SIGNAL_FUNC(closePrompt), windowPrompt);
	gnome_dialog_editable_enters(GNOME_DIALOG(windowPrompt),
 	 					 GTK_EDITABLE(txtFind));

	gtk_window_set_modal(GTK_WINDOW(windowPrompt), TRUE);
	gtk_widget_show(windowPrompt);
	gnome_dialog_set_default(GNOME_DIALOG(windowPrompt),0);
	gtk_widget_grab_focus(txtFind);
}

/******************* SHARE SCANNING ********************/


void passwordPrompt(machineNode * tmp);

/*Scan a machine for a list of shares.  If it fails prompt for a password */
void doShareScan(machineNode * info) {
	gint res;
	g_return_if_fail(info);

	setBusyCursor();
	res = getShareList(info);
	setNormalCursor();

	if (res == 0)			/* we succeeded */
		info->scanned = TRUE;

	if ((res == -5) && (!NoAuthentication))	/*if the user doesn't want to see a password prompt, just give up */
		passwordPrompt(info);
}


/*password for scannig the share*/
void updatePW(GnomeDialog * w, int button, gpointer * data) {
	if (button == 0) {
		machineNode *tmp;
		PWInfo *pwi = (PWInfo *) data;

		tmp = pwi->info;

		if (tmp->username != NULL) {
			g_free(tmp->username);
			tmp->username = NULL;
		}
		tmp->username = g_strdup(gtk_entry_get_text(GTK_ENTRY(pwi->username)));

		if (tmp->passwd != NULL) {
			g_free(tmp->passwd);
			tmp->passwd = NULL;
		}
		tmp->passwd = g_strdup(gtk_entry_get_text(GTK_ENTRY(pwi->passwd)));

		if (tmp->workgroupName != NULL) {
			g_free(tmp->workgroupName);
			tmp->workgroupName = NULL;
		}
		tmp->workgroupName = g_strdup(gtk_entry_get_text(GTK_ENTRY(pwi->workgroup)));

		g_free(pwi);

		doShareScan(tmp);
	}
}

void passwordPrompt(machineNode * tmp) {
	GtkWidget *windowPrompt;
	GtkWidget *label;
	GtkWidget *txtUsername;
	GtkWidget *txtPasswd;
	GtkWidget *txtWorkgroup;
	GtkWidget *vbox;
	GtkWidget *hbox;
	PWInfo *pwi;

	/*create window which prompts for username and password */

	windowPrompt = gnome_dialog_new(_("Authentication"),
		GNOME_STOCK_BUTTON_OK,GNOME_STOCK_BUTTON_CANCEL,NULL);
	gnome_dialog_set_parent(GNOME_DIALOG(windowPrompt),GTK_WINDOW(window));

	vbox = GNOME_DIALOG(windowPrompt)->vbox;

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, GNOME_PAD_SMALL);

	// Create the username prompt and edit box
	//
	label = gtk_label_new(_("Username:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);

	txtUsername = gtk_entry_new();
	if (tmp->username != NULL)
		gtk_entry_set_text(GTK_ENTRY(txtUsername), tmp->username);
	gtk_box_pack_start(GTK_BOX(hbox), txtUsername, FALSE, FALSE, GNOME_PAD_SMALL);
	gnome_dialog_editable_enters(GNOME_DIALOG(windowPrompt), GTK_EDITABLE(txtUsername));

	gtk_widget_show(hbox);
	gtk_widget_show(label);
	gtk_widget_show(txtUsername);

	// create the password prompt and edit box
	//
	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, GNOME_PAD_SMALL);

	label = gtk_label_new(_("password:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);

	txtPasswd = gtk_entry_new();
	gtk_entry_set_visibility(GTK_ENTRY(txtPasswd), FALSE);
	gtk_box_pack_start(GTK_BOX(hbox), txtPasswd, FALSE, FALSE, GNOME_PAD_SMALL);
	gnome_dialog_editable_enters(GNOME_DIALOG(windowPrompt), GTK_EDITABLE(txtPasswd));
	gtk_widget_show(hbox);
	gtk_widget_show(label);
	gtk_widget_show(txtPasswd);


	// create the workgroup prompt and edit box
	//
	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, GNOME_PAD_SMALL);

	label = gtk_label_new(_("workgroup:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);

	txtWorkgroup = gtk_entry_new();
	if (tmp->workgroupName != NULL)
		gtk_entry_set_text(GTK_ENTRY(txtWorkgroup), tmp->workgroupName);
	gtk_box_pack_start(GTK_BOX(hbox), txtWorkgroup, FALSE, FALSE, GNOME_PAD_SMALL);
	gnome_dialog_editable_enters(GNOME_DIALOG(windowPrompt), GTK_EDITABLE(txtWorkgroup));
	gtk_widget_show(hbox);
	gtk_widget_show(label);
	gtk_widget_show(txtWorkgroup);


	// Set up signal callbacks so that we can respond to the users input
	pwi = g_malloc(sizeof(PWInfo));
	pwi->username = txtUsername;
	pwi->passwd = txtPasswd;
 	pwi->workgroup = txtWorkgroup;
	pwi->info = tmp;

	gtk_signal_connect(GTK_OBJECT(windowPrompt), "clicked",
				 GTK_SIGNAL_FUNC(updatePW), pwi);
	gtk_signal_connect(GTK_OBJECT(windowPrompt), "clicked",
				 GTK_SIGNAL_FUNC(closePrompt), windowPrompt);

	// Display the dialog
	//
	gtk_window_set_modal(GTK_WINDOW(windowPrompt), TRUE);
	gtk_widget_show(windowPrompt);
	gnome_dialog_set_default(GNOME_DIALOG(windowPrompt),0);

	// Figure out which edit box should be given the focus initially
	//
	if (DefaultUser && *DefaultUser) {
		gtk_widget_grab_focus(txtPasswd);
		gtk_signal_connect_object(GTK_OBJECT(txtPasswd), "activate",
						GTK_SIGNAL_FUNC(gtk_window_activate_default),
						GTK_OBJECT(windowPrompt));
	} else {
		gtk_widget_grab_focus(txtUsername);
		gtk_signal_connect_object(GTK_OBJECT(txtUsername), "activate",
						GTK_SIGNAL_FUNC(gtk_window_activate_default),
						GTK_OBJECT(windowPrompt));
	}
}



/******************MOUNTING CODE*********************	*/

void tryMount();

/*used to get and place text when using the "browse" button */
typedef struct _browseinfo {
	GtkWidget *text;
	GtkWidget *window;
} browseinfo;


void browse_select(GtkWidget * w, browseinfo * data) {
	gtk_entry_set_text(GTK_ENTRY(data->text),
	 gtk_file_selection_get_filename(GTK_FILE_SELECTION(data->window)));
	gtk_object_destroy(GTK_OBJECT(data->window));
	g_free(data);
}

void browse_cb(GtkWidget * w, gpointer data) {
	GtkWidget *window;
	browseinfo *bi;

	g_return_if_fail(data);
	window = gtk_file_selection_new(_("Select Mount Point"));
	gtk_file_selection_hide_fileop_buttons(GTK_FILE_SELECTION(window));
	gtk_window_position(GTK_WINDOW(window), GTK_WIN_POS_MOUSE);
	gtk_signal_connect(GTK_OBJECT(window), "destroy",
				 GTK_SIGNAL_FUNC(gtk_widget_destroyed),
				 &window);

	bi = g_malloc(sizeof(browseinfo));
	bi->text = (GtkWidget *) data;
	bi->window = window;

	gtk_signal_connect(GTK_OBJECT(GTK_FILE_SELECTION(window)->ok_button),
			"clicked", GTK_SIGNAL_FUNC(browse_select), (gpointer) bi);
	gtk_signal_connect_object(GTK_OBJECT(GTK_FILE_SELECTION(window)->cancel_button),
	"clicked", GTK_SIGNAL_FUNC(gtk_widget_destroy), GTK_OBJECT(window));

	if (gtk_entry_get_text(GTK_ENTRY(data)) != NULL)
		gtk_file_selection_set_filename(GTK_FILE_SELECTION(window),
						gtk_entry_get_text(GTK_ENTRY(data)));

	gtk_widget_show(window);
	gtk_grab_add(window);
}

static void guesttoggle_cb(GtkWidget * widget, gpointer data) {

	PWInfo *pwi;

	g_return_if_fail(data);

	if (laststate == TRUE)
		laststate = FALSE;
	else
		laststate = TRUE;

	pwi = (PWInfo *) data;

	gtk_widget_set_sensitive(pwi->username, laststate);
	gtk_widget_set_sensitive(pwi->passwd, laststate);
}


int createDir() {

	/* we assume if we are there the files doesn't exist*/
	gchar *cmd = (gchar *) NULL;

	if (debug)
		g_print("create dir for //%s/%s\n", currnode->machineName, currnode->shareName);

	cmd = g_strdup_printf("mkdir -p \"%s\"", currnode->mountClean);

	if (debug)
		g_print("exec:%s\n", cmd);
	
	if (system(cmd)) {
		g_free(cmd);
		return FALSE; /* if we have a problem, return FALSE*/
	}
	g_free(cmd);

	currnode->mountCreated = 1;
	return TRUE;
}

void createPromptReply_cb(gint reply, gpointer data) {
	g_return_if_fail(data);
	if (reply == 0) {
		currnode->mountName = data;
		currnode->mountClean = makeClean(data);
		 if (createDir()) tryMount();
		 else ShowError(_("Error creating directory"));
	}
}

void createPrompt(gchar * dir) {

/*	g_return_if_fail(dir); */

	GtkWidget *dialog;
	gchar *question;

	question = g_strdup_printf(_("Directory %s doesn't exist ! Create it ?"), dir);
	dialog = gnome_question_dialog_modal(question, createPromptReply_cb, (gpointer) dir);
	gtk_window_set_position(&(GNOME_DIALOG(dialog)->window), GTK_WIN_POS_MOUSE);
	g_free(question);
}

void updateMount(GnomeDialog * w, int button, gpointer * data) {
	machineNode *tmp;
	PWInfo *pwi;
	struct stat st_buf;
	int stat_err = 0;

	if (button != 0)
		return;

	if (debug)
		printf("Entered into updateMount\n");

	pwi = (PWInfo *) data;
	g_return_if_fail(pwi);

	tmp = pwi->info;

	if (tmp->username != NULL) {
		g_free(tmp->username);
		tmp->username = NULL;
	}
	tmp->username = g_strdup(gtk_entry_get_text(GTK_ENTRY(pwi->username)));

	if (tmp->passwd != NULL) {
		g_free(tmp->passwd);
		tmp->passwd = NULL;
	}
	tmp->passwd = g_strdup(gtk_entry_get_text(GTK_ENTRY(pwi->passwd)));

	if (tmp->mountClean != NULL) {
		g_free(tmp->mountClean);
		tmp->mountClean = NULL;
	}

	if (tmp->mountName != NULL) {
		g_free(tmp->mountName);
		tmp->mountName = NULL;
	}
	
	tmp->mountName = g_strdup(gtk_entry_get_text(GTK_ENTRY(pwi->mount)));
	tmp->mountClean = makeClean(tmp->mountName);
	if (debug) g_print("mountClean=>>%s<<\n",tmp->mountName);
	
/* Check if mount point exist */
	if ((stat_err = stat(tmp->mountName, &st_buf))) {

		if (debug)
			g_print("Dir stat: ret=%d	err=%d\n", stat_err, errno);

		if (errno == ENOENT) {
			if (SilentlyCreateMountPoint)
				if (createDir()) tryMount(); 
				else ShowError(_("Error creating directory"));
			else
				createPrompt(tmp->mountName);
		} else
			switch (errno) {
				case ENOTDIR:
					ShowError(_("The mount point you specify is not a directory"));
					break;
				case ELOOP:
					ShowError(_("Too many symbolic links in the path"));
					break;
				case EACCES:
					ShowError(_("You don't have enough privileges to access this directory"));
					break;
				case ENAMETOOLONG:
					ShowError(_("The directory name is too long"));
					break;
				default:
					ShowError(_("Unknown Error With Directory"));
			}
	
	} else {  // the directory existed
		g_free(pwi);

		if (debug)
			printf("about to launch trymount\n");
		tryMount();
	}
}

void mountShareCmd() {
	docmd = 1;
	mountShare(0);
}

void mountShare(int mode) {				/* 0 = new, 1 = edit */
	GtkWidget *windowPrompt;
	GtkWidget *label;
	GtkWidget *txtUsername;
	GtkWidget *txtPasswd;
	GtkWidget *txtMount;
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *chkbox;
	GtkWidget *cmdbrowse;

	PWInfo *pwi;
	machineNode *tmp;
	struct stat st_buf;

	if (currnode == NULL)
		return;
	if (currnode->shareName == NULL)
		return;
	if ((currnode->mountCreated) && (mode == 0)) {
		printf(_("mounted on:>%s<\n"), currnode->mountName);
		doCommand();
		return;
	}
	if (currnode->shareType == PRINTER_SHARE) {
		ShowError("Can't mount a printer");
		return;
	}
	tmp = currnode;

	/* OK, if we have automount on, then we are going to try that first */
	if ((mode ==0) && AutoMount && domnt) {
		domnt = 0;
		
		/* setup the tmp share*/
		tmp->mountName = (gchar *) g_malloc(sizeof(gchar) *
			(strlen(DefaultMount)+strlen(tmp->workgroupName)+
			strlen(tmp->machineName)+strlen(tmp->shareName)+4));
		sprintf(tmp->mountName,"%s/%s",DefaultMount,tmp->workgroupName);
		tmp->mountClean = makeClean(tmp->mountName);
		if (stat(tmp->mountClean,&st_buf))
			createDir();
		g_free(tmp->mountClean);
		sprintf(tmp->mountName,"%s/%s/%s",DefaultMount,
			tmp->workgroupName,tmp->machineName);
		tmp->mountClean = makeClean(tmp->mountName);
		if (stat(tmp->mountClean,&st_buf))
			createDir();

		laststate = TRUE; /*tell it to try user level auth if it can */
		sprintf(tmp->mountName,"%s/%s/%s/%s",DefaultMount,
			tmp->workgroupName,tmp->machineName,tmp->shareName);
		g_free(tmp->mountClean);
		tmp->mountClean = makeClean(tmp->mountName);
		if (stat(tmp->mountClean,&st_buf) == 0 || createDir()) {
			tryMount();
			return;
		}
	}

	/* create and launch window to prompt for mount point. */
	/* set to call updateMount on "OK"														*/

	/*create window which prompts for mount point username and password */
	/*ok button will call updatePW */

	windowPrompt = gnome_dialog_new(_("Select Mount Point"),
		GNOME_STOCK_BUTTON_OK,GNOME_STOCK_BUTTON_CANCEL,NULL);
	gnome_dialog_set_parent(GNOME_DIALOG(windowPrompt),GTK_WINDOW(window));

	vbox = GNOME_DIALOG(windowPrompt)->vbox;

/** Mount point **/

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, GNOME_PAD_SMALL);

	label = gtk_label_new(_("Mount Point:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);

	txtMount = gtk_entry_new();
	if (tmp->mountName != NULL)
		gtk_entry_set_text(GTK_ENTRY(txtMount), tmp->mountName);
	gtk_box_pack_start(GTK_BOX(hbox), txtMount, FALSE, FALSE, GNOME_PAD_SMALL);
	gnome_dialog_editable_enters(GNOME_DIALOG(windowPrompt), GTK_EDITABLE(txtMount));

	cmdbrowse = gtk_button_new_with_label("...");
	gtk_signal_connect(GTK_OBJECT(cmdbrowse), "clicked",
				 (GtkSignalFunc) browse_cb, txtMount);
	gtk_box_pack_start(GTK_BOX(hbox), cmdbrowse, FALSE, FALSE, GNOME_PAD_SMALL);

	gtk_widget_show(hbox);
	gtk_widget_show(label);
	gtk_widget_show(txtMount);
	gtk_widget_show(cmdbrowse);

/** Password Toggle **/

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, GNOME_PAD_SMALL);

	chkbox = gtk_check_button_new_with_label(_("Provide share level authentication"));
	gtk_box_pack_start(GTK_BOX(hbox), chkbox, FALSE, FALSE, GNOME_PAD_SMALL);

	gtk_widget_show(hbox);
	gtk_widget_show(chkbox);

/** Username prompt **/

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, GNOME_PAD_SMALL);

	label = gtk_label_new(_("Username:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);

	txtUsername = gtk_entry_new();
	if (tmp->username != NULL)
		gtk_entry_set_text(GTK_ENTRY(txtUsername), tmp->username);
	gtk_box_pack_start(GTK_BOX(hbox), txtUsername, FALSE, FALSE, GNOME_PAD_SMALL);
	gnome_dialog_editable_enters(GNOME_DIALOG(windowPrompt), GTK_EDITABLE(txtUsername));
	gtk_widget_show(hbox);
	gtk_widget_show(label);
	gtk_widget_show(txtUsername);

/** Password Prompt **/

	hbox = gtk_hbox_new(FALSE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, GNOME_PAD_SMALL);

	label = gtk_label_new(_("Password:"));
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, GNOME_PAD_SMALL);

	txtPasswd = gtk_entry_new();
	gtk_entry_set_visibility(GTK_ENTRY(txtPasswd), FALSE);
	if (tmp->passwd != NULL)
		gtk_entry_set_text(GTK_ENTRY(txtPasswd), tmp->passwd);
	gtk_box_pack_start(GTK_BOX(hbox), txtPasswd, FALSE, FALSE, GNOME_PAD_SMALL);
	gnome_dialog_editable_enters(GNOME_DIALOG(windowPrompt), GTK_EDITABLE(txtPasswd));
	gtk_widget_show(hbox);
	gtk_widget_show(label);
	gtk_widget_show(txtPasswd);

	pwi = g_malloc(sizeof(PWInfo));
	pwi->username = txtUsername;
	pwi->passwd = txtPasswd;
	pwi->info = tmp;
	pwi->mount = txtMount;

	if (tmp->username == NULL)
		laststate = FALSE;
	else
		laststate = TRUE;
	GTK_TOGGLE_BUTTON(chkbox)->active = laststate;
	gtk_widget_set_sensitive(txtUsername, laststate);
	gtk_widget_set_sensitive(txtPasswd, laststate);

	/* Connect our callbacks to the various signals we're interested in*/
	gtk_signal_connect(GTK_OBJECT(windowPrompt), "clicked",
				 GTK_SIGNAL_FUNC(updateMount), pwi);
	gtk_signal_connect(GTK_OBJECT(windowPrompt), "clicked",
				 GTK_SIGNAL_FUNC(closePrompt), windowPrompt);

	gtk_signal_connect(GTK_OBJECT(chkbox), "clicked",
				 GTK_SIGNAL_FUNC(guesttoggle_cb), pwi);

	gnome_dialog_editable_enters(GNOME_DIALOG(windowPrompt),
															 GTK_EDITABLE(txtMount));

	/*Display the window, set the default button, set the focus to the
	appropriate widget*/
	gtk_window_set_modal(GTK_WINDOW(windowPrompt), TRUE);
	gtk_widget_show(windowPrompt);
	gnome_dialog_set_default(GNOME_DIALOG(windowPrompt),0);
	gtk_widget_grab_focus(txtMount);

}

void tryMount() {
	gint res;
	mountedSmbfs *curr = rootMounted;

	setBusyCursor();
	res = doMount();
	setNormalCursor();

	if (res == -3) {
		mountShare(1);
		return;
	} else if (res == -5) {
		mountShare(1);
		return;
	} else if (res == -1) {
		/* unrecoverable error, give up*/
		return;
	}
	
	currnode->mountCreated = TRUE;

	/*we succeded, but we want to suppress the popup */
	if (res != -7)
		ShowError("Mount Succedded!");

	gtk_ctree_node_set_text(GTK_CTREE(workgrouplist), gtk_ctree_find_by_row_data(GTK_CTREE(workgrouplist), NULL, currnode), 2, currnode->mountName);
	if (curr) {
		while (curr->next)
			curr = curr->next;
		curr->next = g_malloc(sizeof(mountedSmbfs));
		curr = curr->next;
		curr->next = (mountedSmbfs *) NULL;
	} else {
		rootMounted = curr = g_malloc(sizeof(mountedSmbfs));
		curr->next = (mountedSmbfs *) NULL;
	}
	curr->machineName = g_strdup(currnode->machineName);
	curr->shareName = g_strdup(currnode->shareName);
	curr->mountName = g_strdup(currnode->mountName);
	updateActionMenuItems();
}


/* *******************END MOUNTING CODE ***************** */

/* Some callbacks */
static void workgroupSelect(GtkCTree * ctree, GtkCTreeNode * node, gint column, gpointer * data) {
	if (data)
 		currnode = gtk_ctree_node_get_row_data(ctree, node);
	else
		currnode = NULL;
	updateActionMenuItems();
}

static void clist_click_column(GtkCList * clist, gint column, gpointer data) {
	if (column != clist->sort_column)
		gtk_clist_set_sort_column(clist, column);
	else {
		if (clist->sort_type == GTK_SORT_ASCENDING)
			clist->sort_type = GTK_SORT_DESCENDING;
		else
			clist->sort_type = GTK_SORT_ASCENDING;
	}
	gtk_clist_sort(clist);
}

void sortList() {
	GtkCList *clist;

	clist = GTK_CLIST(workgrouplist);

	if (clist->sort_type == GTK_SORT_ASCENDING)
		clist->sort_type = GTK_SORT_DESCENDING;
	else
		clist->sort_type = GTK_SORT_ASCENDING;

	gtk_ctree_sort_node(GTK_CTREE(workgrouplist), gtk_ctree_find_by_row_data(GTK_CTREE(workgrouplist), NULL, currnode));
}

/*menu's for popup */
static GnomeUIInfo unmounted_disc_popup_menu[] =
{
	GNOMEUIINFO_ITEM_NONE(N_("Browse In GMC"), N_("Browse the current share in gmc"), browseShare),
	GNOMEUIINFO_ITEM_NONE(N_("Mount"), N_("Mount"), mountShare),
	GNOMEUIINFO_ITEM_NONE(N_("Mount with command"), N_("Mount with default command"), mountShareCmd),
	GNOMEUIINFO_END
};

static GnomeUIInfo mounted_disc_popup_menu[] =
{
	GNOMEUIINFO_ITEM_NONE(N_("UnMount"), N_("UnMount"), unmountShare),
	GNOMEUIINFO_END
};

static GnomeUIInfo printer_popup_menu[] =
{
	GNOMEUIINFO_ITEM_NONE(N_("Add as network printer"), N_("Add the printer as a network printer"), addNetworkPrinter),
	GNOMEUIINFO_ITEM_NONE(N_("Add as SMB printer"), N_("Add as a samba printer"), addSmbPrinter),
	GNOMEUIINFO_ITEM_NONE(N_("Remove"), N_("Remove printer"), removePrinter),
	GNOMEUIINFO_END
};

static GnomeUIInfo machine_popup_menu[] =
{
GNOMEUIINFO_ITEM_NONE(N_("Refresh"), N_("Refresh Share List"), refreshList),
	GNOMEUIINFO_ITEM_NONE(N_("Sort"), N_("Sort List"), sortList),
	GNOMEUIINFO_END
};

static GnomeUIInfo workgroup_popup_menu[] =
{
	GNOMEUIINFO_ITEM_NONE(N_("Sort"), N_("Sort List"), sortList),
	GNOMEUIINFO_END
};


/*this is where we deal with double clicks or right clicks, both of which only mean something if we are on a share */
gint buttonEvent(GtkWidget * widget, GdkEventButton * event) {
	gint row, column;
	machineNode *mn;
	GtkCTreeNode *ctn;
	GtkWidget *popup;

	if ((event->type == GDK_2BUTTON_PRESS) && (event->button == 1)) {

		gtk_clist_get_selection_info(GTK_CLIST(widget), event->x, event->y,
					 &row, &column);
		mn = gtk_clist_get_row_data(GTK_CLIST(widget), row);
		ctn = gtk_ctree_node_nth(GTK_CTREE(widget), row);
		if (mn == NULL)
			return TRUE;
		else if (mn->machineName == NULL) {
			/* Where does this come from?
			doNetworkScan(mn); */
			gtk_ctree_toggle_expansion(GTK_CTREE(workgrouplist), ctn);
		} else if (mn->shareName == NULL) {
			if (mn->scanned != TRUE) {
				doShareScan(mn);
				gtk_ctree_toggle_expansion(GTK_CTREE(workgrouplist), ctn);
			}
		} else if ((AutoMount)||(AutoCmd)) {
			currnode = mn;
			domnt = 1;
			mountShareCmd();
		}
		return TRUE;
	}

	if ((event->type == GDK_BUTTON_PRESS) && (event->button == 3)) {

		gtk_clist_get_selection_info(GTK_CLIST(widget), event->x, event->y,
				 &row, &column);
		mn = gtk_clist_get_row_data(GTK_CLIST(widget), row);

		ctn = gtk_ctree_node_nth(GTK_CTREE(widget), row);
		gtk_ctree_select(GTK_CTREE(widget), ctn);

		if ((mn == NULL) || (mn->machineName == NULL)) {
			popup = gnome_popup_menu_new(workgroup_popup_menu);
		} else if (mn->shareName == NULL) {
			if (debug)
				g_print("pop up menu, machine:%s\n", mn->machineName);
			popup = gnome_popup_menu_new(machine_popup_menu);
		} else {
			if (debug)
				g_print("pop up menu, share:%s\n", mn->shareName);
			if (mn->shareType == DISC_SHARE)
				popup = gnome_popup_menu_new(mn->mountName ? mounted_disc_popup_menu : unmounted_disc_popup_menu);
			else if (mn->shareType == PRINTER_SHARE)
				popup = gnome_popup_menu_new(printer_popup_menu);
				else popup = NULL;
		}
		gnome_popup_menu_do_popup_modal(popup, NULL, NULL, event, NULL);

		return TRUE;
	}
	return FALSE;
}


/* Put together the main browser screen */
void createBrowser(GtkWidget * frame) {
	GtkWidget *hbox;
	gchar *listTitles[3];
	GdkColor transparent;

	network_pixmap = gdk_pixmap_create_from_xpm_d(window->window, &network_mask,
						&transparent, network_xpm);
	workgroup_pixmap = gdk_pixmap_create_from_xpm_d(window->window, &workgroup_mask,
								 &transparent, workgroup_xpm);
	machine_pixmap = gdk_pixmap_create_from_xpm_d(window->window, &machine_mask,
						&transparent, machine_xpm);
	share_pixmap = gdk_pixmap_create_from_xpm_d(window->window, &share_mask,
								&transparent, share_xpm);
	printer_pixmap = gdk_pixmap_create_from_xpm_d(window->window, &printer_mask,
						&transparent, printer_xpm);

	listTitles[0] = g_strdup(_("Workgroup Name"));
	listTitles[1] = g_strdup(_("Comment"));
	listTitles[2] = g_strdup(_("Mount Point"));

	hbox = gtk_hbox_new(FALSE, 0);
	workgrouplist = gtk_ctree_new_with_titles(3, 0, listTitles);
	gtk_signal_connect(GTK_OBJECT(workgrouplist), "tree_select_row",
			 (GtkSignalFunc) workgroupSelect, (gpointer)TRUE);
	gtk_signal_connect(GTK_OBJECT(workgrouplist), "tree_unselect_row",
			 (GtkSignalFunc) workgroupSelect, (gpointer)FALSE);
	gtk_signal_connect(GTK_OBJECT(workgrouplist), "click_column",
			 (GtkSignalFunc) clist_click_column, NULL);

	workgroupframe = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_add(GTK_CONTAINER(workgroupframe), workgrouplist);
	gtk_box_pack_start(GTK_BOX(hbox), workgroupframe, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(workgroupframe),
				 GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

	gtk_signal_connect(GTK_OBJECT(workgrouplist), "button_press_event",
				 (GtkSignalFunc) buttonEvent, NULL);

	gtk_widget_show(workgroupframe);
	gtk_widget_show(workgrouplist);

	gtk_box_pack_start(GTK_BOX(frame), hbox, TRUE, TRUE, 0);
	gtk_widget_show(hbox);
}

/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
