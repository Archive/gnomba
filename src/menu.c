/* Gnomba
 * Copyright (C) 1999 Gnomba Team
 *
 * menu.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>

#include "menu.h"
#include "net.h"
#include "properties.h"
#include "browser.h"
#include "smbwrap.h"

void file_quit_callback (GtkWidget *widget, gpointer data){gtk_exit(0);}

void actions_expand_callback(GtkWidget *widget, gpointer data){

  GtkCTreeNode * root;
  GtkCTreeNode * curr;
  gint i;
  
  root =  gtk_ctree_node_nth(GTK_CTREE(workgrouplist), 0 );

  i = 1; 
  while ((curr = gtk_ctree_node_nth(GTK_CTREE(workgrouplist), i++ )) != NULL) {

    if (gtk_ctree_is_ancestor(GTK_CTREE(workgrouplist), root, curr))
      gtk_ctree_expand_to_depth(GTK_CTREE(workgrouplist),curr,2);
  }
}

void actions_collapse_callback(GtkWidget *widget, gpointer data){

  GtkCTreeNode * root;
  GtkCTreeNode * curr;
  gint i;
  
  root =  gtk_ctree_node_nth(GTK_CTREE(workgrouplist), 0 );

  i = 1; 
  while ((curr =  gtk_ctree_node_nth(GTK_CTREE(workgrouplist), i++ )) != NULL) {

    if (gtk_ctree_is_ancestor(GTK_CTREE(workgrouplist), root, curr))
      gtk_ctree_collapse_to_depth(GTK_CTREE(workgrouplist),curr,2);
  }
}

void actions_browse_callback(GtkWidget *widget, gpointer data){
  browseShare();
}


void actions_find_callback(GtkWidget *widget, gpointer data){
  doFind();
}


void actions_mount_callback(GtkWidget *widget, gpointer data){
	docmd = 0;
	mountShare(0);
}

void actions_mount_command_callback(GtkWidget *widget, gpointer data){
	docmd = 1;
	mountShare(0);
}

void actions_unmount_callback(GtkWidget *widget, gpointer data){
  unmountShare(0);
}

void actions_unmount_all_callback(GtkWidget *widget, gpointer data){
  unmountAllShares();
}

void options_configuration_callback(GtkWidget *widget, gpointer data){ show_properties_dialog();}


void file_scan_callback( GtkWidget * widget, gpointer data) {
 DoScan();
}

void help_about_callback (GtkWidget *widget, void *data) {
  GtkWidget *about;
  const gchar *authors[] = {
    "Chris Rogers","Brian Nigito",
    NULL
  };

  about = gnome_about_new ( _("gnomba"),
                            VERSION,
                            /* copyrigth notice */
                            _("(C) 1999, 2000"),
                            authors,
                            /* another comments */
                            _("Gnome Samba Browser"),
                            NULL);
  gtk_widget_show (about);
  
  return;
}

static GnomeUIInfo file_menu[]= {
  GNOMEUIINFO_ITEM_NONE(
    N_("(re)_Scan"), N_("Scan"),
    file_scan_callback),
  GNOMEUIINFO_SEPARATOR,
  GNOMEUIINFO_MENU_EXIT_ITEM(file_quit_callback, NULL),
  GNOMEUIINFO_END
};


static GnomeUIInfo action_menu[]=
{
  
  {
    GNOME_APP_UI_ITEM,
    N_("_Expand Workgroups"), N_("Expand all workgroups"),
    actions_expand_callback, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    'A', GDK_CONTROL_MASK, NULL
  }, 
  {
    GNOME_APP_UI_ITEM,
    N_("_Collapse Workgroups"), N_("Collapse all workgroups"),
    actions_collapse_callback, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    'D', GDK_CONTROL_MASK, NULL
  }, 
  {
    GNOME_APP_UI_ITEM,
    N_("_Find"), N_("Find"),
    actions_find_callback, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    'F', GDK_CONTROL_MASK, NULL
  }, 


  GNOMEUIINFO_SEPARATOR, 
  {
    GNOME_APP_UI_ITEM,
    N_("_Browse in GMC"), N_("Browse the current share with GMC"),
    actions_browse_callback, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    'B', GDK_CONTROL_MASK, NULL
  },   
  {
    GNOME_APP_UI_ITEM,
    N_("_Mount share"), N_("Mount the current share"),
    actions_mount_callback, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    'M', GDK_CONTROL_MASK, NULL
  },   
  {
    GNOME_APP_UI_ITEM,
    N_("_Mount share with command"), N_("Mount the current share and execute default command"),
    actions_mount_command_callback, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    'C', GDK_CONTROL_MASK, NULL
  },   
  {
    GNOME_APP_UI_ITEM,
    N_("_Unmount share"), N_("Unmount the current share"),
    actions_unmount_callback, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    'U', GDK_CONTROL_MASK, NULL
  },   
  {
    GNOME_APP_UI_ITEM,
    N_("Unmount _All"), N_("Unmount all shares"),
    actions_unmount_all_callback, NULL, NULL,
    GNOME_APP_PIXMAP_NONE, NULL,
    'U', GDK_CONTROL_MASK, NULL
  },   


  GNOMEUIINFO_END

};

static GnomeUIInfo options_menu[]=
{
	GNOMEUIINFO_MENU_PREFERENCES_ITEM(options_configuration_callback, NULL),
  GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[]=
{
	GNOMEUIINFO_MENU_ABOUT_ITEM(help_about_callback, NULL),
  GNOMEUIINFO_END
};


static GnomeUIInfo main_menu[]= 
{
  GNOMEUIINFO_MENU_FILE_TREE(file_menu),
  GNOMEUIINFO_SUBTREE(N_("_Action"), action_menu),
  GNOMEUIINFO_MENU_SETTINGS_TREE(options_menu),
  GNOMEUIINFO_MENU_HELP_TREE(help_menu),
  GNOMEUIINFO_END
};

void createMenu(GtkWidget *frame) {
  gnome_app_create_menus (GNOME_APP (frame), main_menu);
  gnome_app_install_menu_hints(GNOME_APP(frame), main_menu);
}

void updateActionMenuItems(){
  int i;
  GtkCTreeNode *ctn;
  machineNode *mn = currnode;
  gboolean sensitive;

  sensitive = (mn && mn->shareName && mn->shareType != PRINTER_SHARE && !mn->mountCreated);
  gtk_widget_set_sensitive(action_menu[5].widget,sensitive);
  gtk_widget_set_sensitive(action_menu[6].widget,sensitive);
  sensitive = (mn && mn->shareName && mn->mountCreated);
  gtk_widget_set_sensitive(action_menu[4].widget,sensitive);
  gtk_widget_set_sensitive(action_menu[7].widget,sensitive);

  sensitive = FALSE;
  for (i=0; (ctn = gtk_ctree_node_nth(GTK_CTREE(workgrouplist),i)); i++) {
    mn = (machineNode*)gtk_ctree_node_get_row_data(GTK_CTREE(workgrouplist),ctn);
    if (mn && mn->mountCreated) {
      sensitive = TRUE;
      break;
    }
  }
  gtk_widget_set_sensitive(action_menu[8].widget,sensitive);
}

/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
