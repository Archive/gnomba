/*
// NetBios Headers
*/

#ifndef _NMBHDR_H__
#define _NMBHDR_H__

#define NMBHDRSIZE 13


struct nmbhdr {
	unsigned short id; /* name_trn_id */

	unsigned char R:1; /* response (BOOL) */
	unsigned char opcode:4; /* opcode - could be switched with above?
*/
	unsigned char AA:1; /* broadcast (BOOL) */
	unsigned char TC:1; /* truncate (BOOL) */
	unsigned char RD:1; /* recursion desired */
	unsigned char RA:1; /* recursion available */
	unsigned char unless:2;
	unsigned char B:1;
	unsigned char RCODE:4;

	unsigned short que_num;
	unsigned short rep_num;
	unsigned short num_rr;
	unsigned short num_rrsup;
	unsigned char namelen;
};


struct typez {
    unsigned short type;
    unsigned short type2;
};

#endif /* _NMBHDR_H__ */


/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
