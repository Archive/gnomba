/* Gnomba - Gnome Samba Browser
 * Copyright (C) 1999 Gnomba Team
 *
 * gnomba.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "config.h"
#include <gnome.h>
#include "gnomba.h"
#include "mtab.h"

void
cleanUpMountedSmbfs(mountedSmbfs **root_mounted)
{
  mountedSmbfs *curr = *root_mounted; 
  mountedSmbfs *root = *root_mounted; 

  *root_mounted = (mountedSmbfs *)NULL;
  while ( curr )
  {
    root = curr; 
    curr = curr->next;
    if ( root->machineName )
    {
      g_free( (gpointer)root->machineName );
      root->machineName =(gchar *)NULL;
    }
    if ( root->shareName )
    {
      g_free( (gpointer)root->shareName );
      root->shareName =(gchar *)NULL;
    }
    if ( root->mountName )
    {
      g_free( (gpointer)root->mountName );
      root->mountName =(gchar *)NULL;
    }
    root->next =(mountedSmbfs *)NULL; 
  }
}

void
refreshMountedByHost(mountedSmbfs **root_mounted, gchar *machine_name)
{
  /* We build a temporary list to get machine shares */
  mountedSmbfs *root_tmp = doScanMountedSmbfs();
  mountedSmbfs *curr_tmp = root_tmp; 
  /* Original list */
  mountedSmbfs *curr_orig = *root_mounted;

  /* Go to the end of original list */
  if ( curr_orig )
    while ( curr_orig->next )
      curr_orig = curr_orig->next; 
  
  /* search the new list for required host */
  while ( curr_tmp )
  {
    if ( !strcasecmp( curr_tmp->machineName, machine_name ) )
    {
      mountedSmbfs *copy;
      
#ifdef DEBUG
      if ( debug )
        g_print( "Adding mount point %s for %s\n", curr_tmp->mountName, machine_name ); 
#endif

      /* copy the node */
      copy = g_malloc( sizeof( mountedSmbfs ) );
      copy->machineName = g_strdup( curr_tmp->machineName ); 
      copy->shareName = g_strdup( curr_tmp->shareName ); 
      copy->mountName = g_strdup( curr_tmp->mountName );
      copy->next = NULL;

      /* Link the new node */
      if ( curr_orig )
      {
        curr_orig->next = copy;
        curr_orig = curr_orig->next;
      }
      else
      {
        *root_mounted = copy;
        curr_orig = *root_mounted;
      }
    }

    curr_tmp = curr_tmp->next; 
  }

  /* Cleanup the tmp list */
  cleanUpMountedSmbfs( &root_tmp ); 
}

mountedSmbfs *
doScanMountedSmbfs()
{
  FILE *mtabfp;
  gchar buf[1024];
  mountedSmbfs *root =(mountedSmbfs *)NULL, *curr =(mountedSmbfs *)NULL; 

  mtabfp = fopen( "/etc/mtab", "r" );
  if ( !mtabfp ) return NULL; 
  while ( fgets( buf, 1023, mtabfp ) )
  {
    gchar machine[256];
    gchar * share;
    gchar mount[256];
    gchar type[20];
    gint backup, check;

    if ( sscanf( buf, "//%s %s %s %d %d", machine, mount, type, &backup, &check ) == 5 )
      if ( !strncmp( type, "smbfs", strlen( "smbfs" ) ) )
      {
        share = strchr( machine, '/' );
        *share = 0;
        share++; 
#ifdef DEBUG
        if ( debug )
        {
          g_print( "found mounted fs: %s\n", buf );
          g_print( "       machine: %s\n", machine ); 
          g_print( "       share: %s\n", share ); 
          g_print( "       mount: %s\n", mount ); 
          g_print( "       type: %s\n", type ); 
        }
#endif
        if ( curr )
        {
          curr->next = g_malloc( sizeof( mountedSmbfs ) );
          curr = curr->next;
          curr->next = (mountedSmbfs *)NULL;
        }
        else
        {
          curr = root = g_malloc( sizeof( mountedSmbfs ) );
          curr->next = (mountedSmbfs *)NULL;
        }
        curr->machineName = g_strdup( machine );
        curr->shareName = g_strdup( share ); 
        curr->mountName = g_strdup( mount ); 
      }
  }
  fclose( mtabfp ); 
  return root; 
}


/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
