/* Gnomba
 * Copyright (C) 1999 Gnomba Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *	
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the	
 * GNU General Public License for more details.
 *	
 * You should have received a copy of the GNU General Public License
 * Along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	
 * 02111-1307, USA.
 */


/*stuff for smbclient */
#include "smbwrap.h"
#include "smbscan.h"
#include "error.h"
#include "browser.h"
#include "net.h"
#include "mtab.h"
#include "statusbar.h"
#include "menu.h"
#include <util.h>

#include <sys/ioctl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <unistd.h>
#include <fcntl.h>

extern int errno;

#define MAX_BUF				9128

int getShareList(machineNode * tmp) {

	//ok our standard one now			
	int stout[2];
	int sterr[2];
	int ssout;

	char buf[MAX_BUF];		/*ok we should make this dynamic, but how many shares you want anyways? <g> */
	gint rval, rval2;
	int fd;
	int row;
	FILE *fl;
	char *cmd;
	char *lne, *end;
	char *share, *type, *comment;
	int maxl;
	uint32 typen;
	
	//wow, hacks ville, population, me.  
	if ( ScanType == SMBSCAN) return getSmbShareList(tmp);
	
	if (pipe(stout) == -1) {
		if (debug)
			g_print("errno = %d\n", errno);
		ShowError(_("Error creating pipes"));
		return (-1);
	}
	if (pipe(sterr) == -1) {
		if (debug)
			g_print("errno = %d\n", errno);
		ShowError(_("Error creating pipes"));
		return (-1);
	}
	ssout = dup(STDOUT_FILENO);
	dup2(stout[1], STDOUT_FILENO);
	dup2(sterr[1], STDERR_FILENO);

	if ((fd = open("/dev/tty", O_RDWR)) >= 0) {
		if (ioctl(fd, TIOCNOTTY) < 0) {
			if (debug)
	g_print("errno = %d\n", errno);
			ShowError(_("Error Setting up /dev/tty"));
			close(fd);
			return (-1);
		}
	}

	if (tmp->username == NULL) {
		if (tmp->workgroupName == NULL) {
			cmd = g_strdup_printf("smbclient -L \"%s\" -U guest %s %s", tmp->machineName, strlen(tmp->ip) ? "-I" : "", tmp->ip);
		} else {		
			cmd = g_strdup_printf("smbclient -L \"%s\" -W \"%s\" -U guest %s %s", 
							tmp->machineName, tmp->workgroupName, strlen(tmp->ip) ? "-I" : "", tmp->ip);
		}
	} else {
		if (tmp->workgroupName == NULL) {
			cmd = g_strdup_printf("smbclient -L \"%s\" -U \"%s\" %s %s", 
							tmp->machineName, tmp->username, strlen(tmp->ip) ? "-I" : "", tmp->ip);
		} else {
			cmd = g_strdup_printf("smbclient -L \"%s\" -W \"%s\" -U \"%s\" %s %s", 
							tmp->machineName, tmp->workgroupName, tmp->username, strlen(tmp->ip) ? "-I" : "", tmp->ip);
		}
	}

	if (debug)
		g_print("running:%s\n", cmd);

	fl = popen(cmd, "w");

	if (tmp->passwd == NULL)
		fprintf(fl, "\n");
	else
		fprintf(fl, "%s\n", tmp->passwd);

	rval2 = pclose(fl);

	if ((rval = read(stout[0], buf, MAX_BUF - 1)) < 0)
		g_print("errno = %d\n", errno);

	buf[rval - 1] = '\n';
	buf[rval] = '\0';
	dup2(ssout, STDOUT_FILENO);	/*fix stdio */
	g_free(cmd);

	if (rval2 == 32512) {		/*This return value seems to indicate that we couldn't find it */
		ShowError(_("smbclient was not found. Make sure you have it\ninstalled and it is in your path"));
		return -1;
	}
	if (rval2 == 32256) {		/*This seems to mean the perms are wrong */
		ShowError(_("Could not execute smbclient. Make sure it has\ncorrect permissions for you to execute it"));
		return -1;
	}
	if (debug)
		g_print("%s", buf);

	if (rval2 == 256) {		/*if we can't conenct, quit */
		if (debug)
			g_print("Cannot connect:%d\n", rval2);
		return -5;
	}
	/*Parse the Data */

	gtk_clist_freeze(GTK_CLIST(workgrouplist));

	lne = buf;
	end = strchr(lne, '\n');
	*end = '\0';
	/* a little paranoi is healthy */
	maxl = sizeof(gchar) * (strlen(lne) + 1);
	share = g_malloc0(maxl);
	type = g_malloc0(maxl);
	comment = g_malloc0(maxl);

	sscanf(lne, "%s %s %[^9]", share, type, comment);
	while (strcmp(share, "---------") != 0) {

		g_free(share);
		g_free(type);
		g_free(comment);
		if (debug)
			g_print("loop: %s\n", lne);
		lne = end + 1;
		end = strchr(lne, '\n');
		*end = '\0';

		maxl = sizeof(gchar) * (strlen(lne) + 1);
		share = g_malloc0(maxl);
		type = g_malloc0(maxl);
		comment = g_malloc0(maxl);
		sscanf(lne, "%s %s %[^9]", share, type, comment);
	}

	g_free(share);
	g_free(type);
	g_free(comment);
	row = 0;
	while (*(end + 1) != '\n') {
		lne = end + 1;
		end = strchr(lne, '\n');
		*end = '\0';
		if (!end)
			break;

		if (debug)
			g_print("scan: %s\n", lne);

		maxl = sizeof(gchar) * (strlen(lne) + 1);
		share = g_malloc0(maxl);
		type = g_malloc0(maxl);
		comment = g_malloc0(maxl);

		sscanf(lne, "\t%15[^~] %s %[^~]", share, type, comment);

		trim(share);
		trim(type);
		trim(comment);

		//convert type to number
		if (!strcmp(type, "Printer")) typen = PRINTER_SHARE;
		else if (!strcmp(type, "IPC$")) typen = IPC_SHARE;
		else typen = DISC_SHARE;

		addShare(share, typen, comment);
		g_free(share);
		g_free(type);
		g_free(comment);
	}
	gtk_clist_thaw(GTK_CLIST(workgrouplist));
	gtk_clist_columns_autosize(GTK_CLIST(workgrouplist));
	return 0;
}

	
void addShare(const char *name, uint32 type, const char *comment) {
	
	GList *node;
	machineNode *mn;
	GtkStyle *nodeStyle;
	mountedSmbfs *mntent, *prevmntent;
	char *text[3];
	
	g_print("Share: %s comment: %s  type:%d\n", name, comment, type);
			
	if ((!((HideIPC) && (type == IPC_SHARE) )) &&
	(!((HideDollars) && (name[strlen(name) - 1] == '$')))) {

		text[0] = (char*)name;
		text[1] = (char*)comment;
		text[2] = g_malloc0(sizeof(gchar) * 1);
		
		if (type == PRINTER_SHARE) {
			node = (GList *) gtk_ctree_insert_node(GTK_CTREE(workgrouplist),
					 GTK_CTREE_NODE(currnode->machine), NULL, text,
							5, printer_pixmap, printer_mask, printer_pixmap, printer_mask, TRUE, FALSE);
		} else {
			node = (GList *) gtk_ctree_insert_node(GTK_CTREE(workgrouplist),
						 GTK_CTREE_NODE(currnode->machine), NULL, text,
							5, share_pixmap, share_mask, share_pixmap, share_mask, TRUE, FALSE);
		}
			
		/* set net font */
		nodeStyle = gtk_style_copy(gtk_widget_get_style(GTK_WIDGET(workgrouplist)));
		nodeStyle->font = gdk_font_load(NetFont);
		gtk_ctree_node_set_cell_style(GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), 0, nodeStyle);

		/* set comment font */
		nodeStyle = gtk_style_copy(gtk_widget_get_style(GTK_WIDGET(workgrouplist)));
		nodeStyle->font = gdk_font_load(CommentFont);
		gtk_ctree_node_set_cell_style(GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), 1, nodeStyle);

		/* set mount font */
		nodeStyle = gtk_style_copy(gtk_widget_get_style(GTK_WIDGET(workgrouplist)));
		nodeStyle->font = gdk_font_load(MountFont);
		gtk_ctree_node_set_cell_style(GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), 2, nodeStyle);

		mn = copyMN(currnode);
		mn->shareName = g_strdup(name);
		mn->shareClean = makeClean(mn->shareName);
		mn->shareType = type;
		
		mntent = rootMounted;
		prevmntent = (mountedSmbfs *) NULL;
		while (mntent) {
			if (debug)
				g_print("testing mounted smbfs %s %s %s\n", mntent->machineName, mntent->shareName, mntent->mountName);
			
			if (!strncasecmp(mntent->machineName, mn->machineName, strlen(mn->machineName)) &&
				!strncasecmp(mntent->shareName, mn->shareName, strlen(mn->shareName))) {
				
				mn->mountName = mntent->mountName;
				mn->mountClean = makeClean(mntent->mountName);
				if (prevmntent)
					prevmntent->next = mntent->next;
				else
					rootMounted = mntent->next;
				g_free(mntent->machineName);
				/* Hope mountName will be correctly cleaned up */
				g_free(mntent->shareName);
				g_free(mntent);
				break;
			}
			prevmntent = mntent;
			mntent = mntent->next;
		}
		gtk_ctree_node_set_text(GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), 2, mn->mountName);
		gtk_ctree_node_set_row_data(GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), mn);
		g_free(text[2]);
	}
}


void browseShare() {

	gchar *cmd;
	machineNode *mn;

	mn = currnode;

	if ((mn == NULL) || (mn->shareName == NULL))
		return;

	cmd = g_strdup_printf("gmc \"/#smb:%s/%s\"", mn->machineName, mn->shareClean);
	if (debug)
		g_print("view:%s\n", cmd);
	system(cmd);
	g_free(cmd);
}

int doMount() {
	machineNode *tmp;
	gchar *cmd;
	int stout[2];
	int sterr[2];
	int ssout;
	gint rval;
	int fd;
	FILE *fl;

	tmp = currnode;
	//assume that the directory already exists
	
	if (OldSamba) {
		cmd = g_strdup_printf("smbmount \"\\\\\\\\%s\\\\%s\" -W \"%s\" -U \"%s\" %s %s -c 'mount \"%s\"'",
			tmp->machineName, tmp->shareClean, tmp->workgroupName,
			(((tmp->username == NULL) || (laststate == FALSE)) ? "guest" : tmp->username),
			strlen(tmp->ip) ? "-I" : "", tmp->ip, tmp->mountClean);
	} else if (NewSamba){ 
		cmd = g_strdup_printf("smbmount \"//%s/%s\" \"%s\" %s %s -o workgroup=\"%s\",username=\"%s\"",
			tmp->machineName, tmp->shareClean,
			tmp->mountClean, 
			strlen(tmp->ip) ? "-I" : "", tmp->ip, 
			tmp->workgroupName, (((tmp->username == NULL) || (laststate == FALSE)) ? "guest" : tmp->username)
			);
	} else {
		cmd = g_strdup_printf("smbmount \"//%s/%s\" \"%s\" -W \"%s\" -U \"%s\" %s %s	",
			tmp->machineName, tmp->shareClean,
			tmp->mountClean, tmp->workgroupName, 
			(((tmp->username == NULL) || (laststate == FALSE)) ? "guest" : tmp->username),
			strlen(tmp->ip) ? "-I" : "", tmp->ip);
	}
		if (debug) g_print("exec:%s\n",cmd);

	/*jump through loops to avoid using the password in the command */

	if (pipe(stout) == -1) {
		if (debug) g_print("errno = %d\n", errno);
		ShowError(_("Error creating pipes"));
		return (-1);
	}
	if (pipe(sterr) == -1) {
		if (debug) g_print("errno = %d\n", errno);
		ShowError(_("Error creating pipes"));
		return (-1);
	}
	ssout = dup(STDOUT_FILENO);
	dup2(stout[1], STDOUT_FILENO);
	dup2(sterr[1], STDERR_FILENO);

	if ((fd = open("/dev/tty", O_RDWR)) >= 0) {
		if (ioctl(fd, TIOCNOTTY) < 0)
			exit(1);
		close(fd);
	}

	fl = popen(cmd, "w");
	g_free(cmd);

	if (tmp->passwd == NULL)
		fprintf(fl, "\n");
	else
		fprintf(fl, "%s\n", tmp->passwd);

	rval = pclose(fl);
	dup2(ssout, STDOUT_FILENO);	/*fix stdio */

	if (rval == 32512) {		/*This return value seems to indicate that we couldn't find it */
		ShowError(_("smbmount was not found. Make sure you have it\ninstalled and it is in your path"));
		return -1;
	}
	if (rval == 32256) {		/*This seems to mean the perms are wrong */
		ShowError(_("Could not execute smbmount. Make sure it has\ncorrect permissions for you to execute it"));
		return -1;
	}
	
	/*execute the command either because we selected "mount with command" or because we have autmount and autocmd on */

	if (docmd) {
		doCommand();
		docmd = 0;
		return -7;
	}
	return 0;
}

void refreshList()
{
	int start, i = 0;
	GtkCTreeNode *ctn, *rt;
	machineNode *mn;
	gchar *tstr = (gchar *) NULL;

	rt = gtk_ctree_find_by_row_data(GTK_CTREE(workgrouplist), NULL, currnode);

	/*find the row number to work around gtk_ctreeis_ansestor bug */
	while (rt != gtk_ctree_node_nth(GTK_CTREE(workgrouplist), i))
		i++;
	g_print("start is at:%d\n", i);
	start = i;
	i = 0;

	while ((ctn = gtk_ctree_node_nth(GTK_CTREE(workgrouplist), i)) != NULL) {
		mn = (machineNode *) gtk_ctree_node_get_row_data(GTK_CTREE(workgrouplist), ctn);
		if (mn == NULL) {
			tstr = g_strdup("Noname");
		} else if (mn->shareName != NULL)
			tstr = mn->shareName;
		else if (mn->machineName != NULL)
			tstr = mn->machineName;
		else if (mn->workgroupName != NULL)
			tstr = mn->workgroupName;
		else {
			tstr = g_strdup("Error");
		}
		if (debug)
			g_print("scanning:%s(%d) ....\n", tstr, i);
		if (gtk_ctree_is_ancestor(GTK_CTREE(workgrouplist), rt, ctn)) {
			if (i < start)
	break;			/*For some reason if it has no ancestors, it calls everything an ancestor */
			gtk_ctree_remove_node(GTK_CTREE(workgrouplist), ctn);
			if (debug)
	g_print("deleting:%s has an ancestor of %s\n", tstr, currnode->machineName);
			i = 0;			/*deleting changes the numbering, so we have to start over */
		}
		i++;
	}
	getShareList(currnode);
	gtk_ctree_expand(GTK_CTREE(workgrouplist), rt);
}

void unmountAllShares(){
/*for each item, if it has something mounted, 
 * set the current and call unmount
 * remember to set and then reset it though
 */

	machineNode *saveCurr;
	GtkCTreeNode *ctn;
	int i = 0;
	
	saveCurr = currnode;

	while ((ctn = gtk_ctree_node_nth(GTK_CTREE(workgrouplist), i)) != NULL) {
		currnode = (machineNode *) gtk_ctree_node_get_row_data(GTK_CTREE(workgrouplist), ctn);
		if ((currnode != NULL)&&(currnode->mountName != NULL)) {
			unmountShare();
	
		}
	i++;
	}
	currnode = saveCurr;
}

void unmountShare() {

	machineNode *mn;
	gchar *cmd;
	gint res;
	mountedSmbfs *mntent = rootMounted;
	mountedSmbfs *prevmntent = NULL;

	if (!currnode)
		return;
	mn = currnode;
	if ((mn == NULL) || (mn->mountClean == NULL))
		return;

	cmd = g_strdup_printf("smbumount \"%s\"", mn->mountClean);
	if (debug)
		printf("%s\n", cmd);

	res = system(cmd);
	g_free(cmd);

	currnode->mountCreated = FALSE;
	
	if (res != 0) {
		ShowError(_("Error unmounting, please make sure that you have smbumount installed\nand that it is suid root if you are a normal user"));
		return;
	}
	while (mntent) {
		if (!strcmp(mntent->machineName, mn->machineName) && !strcmp(mntent->shareName, mn->shareName)) {
			if (prevmntent)
				prevmntent->next = mntent->next;
			else
				rootMounted = mntent->next;
			g_free(mntent->machineName);
			g_free(mntent->shareName);
			g_free(mntent);
			break;
		}
		prevmntent = mntent;
		mntent = mntent->next;
	}

	cmd = g_strdup_printf("Unmounted %s.", mn->mountName);
	/* This is just obnoxious, so I am removing */
	/*ShowError(cmd);*/

	if (mn->mountCreated && !DontRemoveCreatedMount) {

		gint res = rmdir( mn->mountName );
				 
		if (debug) 
			g_print( "Remove dir %s: ret=%d (error=%d)\n", mn->mountName, res, errno );
	}
	
	g_free(mn->mountName);
	mn->mountName = NULL;
	g_free(mn->mountClean);
	mn->mountClean = NULL;
	gtk_ctree_node_set_text(GTK_CTREE(workgrouplist), gtk_ctree_find_by_row_data(GTK_CTREE(workgrouplist), NULL, mn), 2, "");
	updateActionMenuItems();
}


/*
 * Printer management code
 */
void addNetworkPrinter() {
	machineNode *mn;

	if( ! currnode ) return; 
	mn = currnode;

	ShowError(_("addNetworkPrinter: Sorry ! Not yet implemented !")); 
}

void addSmbPrinter() {
	machineNode *mn;

	if( ! currnode ) return; 
	mn = currnode;

	ShowError(_("addSmbPrinter: Sorry ! Not yet implemented !")); 
}

void removePrinter() {
	machineNode *mn;

	if( ! currnode ) return; 
	mn = currnode;

	ShowError(_("removePrinter: Sorry ! Not yet implemented !")); 
}


/*scan code */

gint HashCompare(gconstpointer sName1, gconstpointer sName2) {
	return (!strcmp((char *) sName1, (char *) sName2));
}

guint HashFunction(gconstpointer key) {
	const char *sKey;
	guint giHashValue = 0;
	int nIndex;

	sKey = key;

	if (key == NULL)
		return (0);

	for (nIndex = 0; nIndex < strlen(sKey); nIndex++) {
		giHashValue = (giHashValue << 4) + (giHashValue ^ (guint) sKey[nIndex]);
	}
	return (giHashValue);
}

void DoScan() {
	machineNode *curr;
	machineNode *root = NULL;
	GList *node, *subtree, *rootnode;
	GHashTable *groupsHash;
	gchar *text[3];
	GtkStyle *nodeStyle1;
	GtkStyle *nodeStyle2;
	GtkStyle *nodeStyle3;

	if ((ScanType == IPSCAN) && (gnome_config_get_int("gnomba/Scan/Max=-1") < 1)) {
		ShowError(_("Before scanning you must specify an IP range to scan.\nThis is done via the Options|Preferences menu, on the Configuration tab."));
		return;
	}
	groupsHash = g_hash_table_new(HashFunction, HashCompare);

	setStatusbar(_("scanning..."));
	gtk_ctree_remove_node(GTK_CTREE(workgrouplist), NULL);
	setBusyCursor();

	/* Scan for mounted smbfs */
	cleanUpMountedSmbfs( &rootMounted );
	rootMounted = doScanMountedSmbfs();

	root = netscan();
	if (debug)
		g_print("returned from scan\n");
	curr = root->next;		/*the first is always empty */

	gtk_clist_freeze(GTK_CLIST(workgrouplist));

	text[0] = g_strdup(_("Network"));
	text[1] = g_new0(gchar, 1);
	text[2] = g_new0(gchar, 1);

	rootnode = (GList *) gtk_ctree_insert_node(GTK_CTREE(workgrouplist), NULL, NULL, text, 5, network_pixmap, network_mask, network_pixmap, network_mask, FALSE, TRUE);

	/* set net font */
	nodeStyle1 = gtk_style_copy( gtk_widget_get_style( GTK_WIDGET(workgrouplist) ) );
	nodeStyle1->font = gdk_font_load( NetFont ); 
	gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(rootnode), 0, nodeStyle1 );

	/* set comment font */
	nodeStyle2 = gtk_style_copy( gtk_widget_get_style( GTK_WIDGET(workgrouplist) ) );
	nodeStyle2->font = gdk_font_load( CommentFont ); 
	gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(rootnode), 1, nodeStyle2 ); 

	/* set mount font */
	nodeStyle3 = gtk_style_copy( gtk_widget_get_style( GTK_WIDGET(workgrouplist) ) );
	nodeStyle3->font = gdk_font_load( MountFont ); 
	gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(rootnode), 2, nodeStyle3 ); 
	
	
	g_free(text[0]); text[0] = NULL;
	g_free(text[1]); text[1] = NULL;
	g_free(text[2]); text[2] = NULL;

	while (curr) {

		/*add to workgrouplist */

		/*first check the hashtable to see if workgroup exists */
		subtree = g_hash_table_lookup(groupsHash, curr->workgroupName);
		if (subtree != NULL) {
			/*if it does, then use it */
			text[0] = curr->machineName; 
			text[1] = g_malloc0(sizeof(gchar) * 1); 
			text[2] = g_malloc0(sizeof(gchar) * 1);

			node = (GList *) gtk_ctree_insert_node(GTK_CTREE(workgrouplist), GTK_CTREE_NODE(subtree), NULL, text, 5, machine_pixmap, machine_mask, machine_pixmap, machine_mask, FALSE, TRUE);
			
			/* set fonts */
			gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), 0, nodeStyle1 );
			gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), 1, nodeStyle2 ); 
			gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), 2, nodeStyle3 ); 
			 
			
			g_free(text[1]); text[1] = NULL;
			g_free(text[2]); text[2] = NULL;

			curr->shareName = NULL;
			curr->machine = node;
			gtk_ctree_node_set_row_data(GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), curr);
		} else {
			/*otherwise create a newone */
			text[0] = curr->workgroupName;
			text[1] = g_malloc0(sizeof(gchar) * 1);
			text[2] = g_malloc0(sizeof(gchar) * 1);

			subtree = (GList *) gtk_ctree_insert_node(GTK_CTREE(workgrouplist), GTK_CTREE_NODE(rootnode), NULL, text, 5, workgroup_pixmap, workgroup_mask, workgroup_pixmap, workgroup_mask, FALSE, TRUE);
			
			/* set fonts */
			gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(subtree), 0, nodeStyle1 );
			gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(subtree), 1, nodeStyle2 ); 
			gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(subtree), 2, nodeStyle3 ); 
			 
		 
			
			g_free(text[1]); text[1] = NULL;
			g_free(text[2]); text[2] = NULL;

			g_hash_table_insert(groupsHash, curr->workgroupName, subtree);
			if (curr->machineName == NULL) {
				//if we are an empty workgroup then add the record here so we can scan it later
				gtk_ctree_node_set_row_data(GTK_CTREE(workgrouplist), GTK_CTREE_NODE(subtree), curr);
			} else {
				//otherwise we want to insert out machine into the group we just made
				
				/*now use it */
				text[0] = curr->machineName;
				text[1] = g_malloc0(sizeof(gchar) * 1);
				text[2] = g_malloc0(sizeof(gchar) * 1);

				node = (GList *) gtk_ctree_insert_node(GTK_CTREE(workgrouplist), GTK_CTREE_NODE(subtree), NULL,
						text, 5, machine_pixmap, machine_mask, machine_pixmap, machine_mask, FALSE, TRUE);
			
				/* set fonts */
				gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), 0, nodeStyle1 );
				gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), 1, nodeStyle2 ); 
				gtk_ctree_node_set_cell_style( GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), 2, nodeStyle3 ); 
			 
				g_free(text[1]); text[1] = NULL;
				g_free(text[2]); text[2] = NULL;

				curr->shareName = NULL;
				curr->machine = node;
				gtk_ctree_node_set_row_data(GTK_CTREE(workgrouplist), GTK_CTREE_NODE(node), curr);
			}
		}
		curr = curr->next;
	}

	gtk_clist_thaw(GTK_CLIST(workgrouplist));
	gtk_clist_columns_autosize(GTK_CLIST(workgrouplist));

	setNormalCursor();
	g_hash_table_destroy(groupsHash);
	clearStatusbar();

}

int doCommand() {

	gchar *cmd;

	/*create the command and execute it */
	if (currnode == NULL)
		return -1;
	if (currnode->mountName == NULL)
		return -1;
	if (DefaultCommand == NULL)
		return -1;
	cmd = g_strdup_printf("%s %s", DefaultCommand, currnode->mountClean);
	if (debug)
		g_print("about to execute command:>%s<\n", cmd);

	return (gnome_execute_shell(NULL, cmd));
}


/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
