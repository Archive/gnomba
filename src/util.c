/* Gnomba
 * Copyright (C) 1999 Gnomba Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *	
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	See the	
 * GNU General Public License for more details.
 *	
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA	
 * 02111-1307, USA.
 */

#include "util.h"
#include <ctype.h>

void setBusyCursor() {
	GdkCursor *cursor;
	cursor = gdk_cursor_new(GDK_WATCH);
	gdk_window_set_cursor (window->window, cursor);
	gdk_cursor_destroy (cursor);
	while (gtk_events_pending()) gtk_main_iteration();
}

void setNormalCursor() {
	GdkCursor *cursor;
	cursor = gdk_cursor_new (GDK_TOP_LEFT_ARROW);
	gdk_window_set_cursor (window->window, cursor);
	gdk_cursor_destroy (cursor);
}
	
gchar * makeClean(gchar * name) {
 
	gchar * tmpn;
	int i, j = 0;
	
	tmpn = malloc(sizeof(gchar)* ((strlen(name) * 2) + 1));
	for (i = 0; i < strlen(name); i++) {
		if (!isalnum(name[i]) && !strchr("/_- .",name[i])) { /* lets escape everything else*/
				tmpn[j++] = '\\';
		}
		tmpn[j++] = name[i];
	}
	tmpn[j] = '\0';
	realloc(tmpn,sizeof(gchar)*(strlen(tmpn)+1));
	return (tmpn);
}

void trim(gchar * str) {
	int i;
	if (str == NULL)
		return;
	for (i = strlen(str) - 1; i >= 0; i--)
		if (str[i] != ' ' && str[i] != '\t')	break;
	str[i + 1] = '\0';
}

machineNode *copyMN(machineNode * mn) {
  machineNode *tmp;

  g_return_val_if_fail(mn, NULL);

  tmp = g_malloc(sizeof(machineNode));

  if (mn->machineName != NULL) {
    tmp->machineName = g_strdup(mn->machineName);
  } else
    tmp->machineName = NULL;

  if (mn->workgroupName != NULL) {
    tmp->workgroupName = g_strdup(mn->workgroupName);
  } else
    tmp->workgroupName = NULL;

  if (mn->ip != NULL) {
    tmp->ip = g_strdup(mn->ip);
  } else
    tmp->ip = NULL;

  if (mn->username != NULL) {
    tmp->username = g_strdup(mn->username);
  } else
    tmp->username = NULL;

  if (mn->passwd != NULL) {
    tmp->passwd = g_strdup(mn->passwd);
  } else
    tmp->passwd = NULL;

  tmp->mountClean = NULL;
  tmp->mountName = NULL;
  tmp->shareType = mn->shareType;
  tmp->mountCreated = mn->mountCreated;

  return tmp;
}


/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
