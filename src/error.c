/* gnomba - Gnome Samba Browser
 * Copyright (C) 1999 Chris Rogers, Brian Nigito
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "error.h"


void ShowError(gchar *emsg) {

  GtkWidget * dialog;
  
  dialog = gnome_ok_dialog (emsg);
  gtk_window_set_position( & (GNOME_DIALOG( dialog )->window), GTK_WIN_POS_MOUSE ); 
  gnome_dialog_run_and_close( GNOME_DIALOG(dialog) );


}

/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
