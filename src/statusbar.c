/* gnomba
 * Copyright (C) 1999 Chris Rogers
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option) 
 * any later version.
 *  
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of 
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
 * GNU General Public License for more details.
 *  
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  
 * 02111-1307, USA.
 */

#include "statusbar.h"
#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-app-helper.h>

void createStatusbar(GtkWidget *frame){
  statusbar = gnome_appbar_new(FALSE, TRUE, GNOME_PREFERENCES_NEVER);
  gnome_app_set_statusbar(GNOME_APP(frame), statusbar);
}

void clearStatusbar(){
  gnome_appbar_pop(GNOME_APPBAR(statusbar));
}

void setStatusbar(gchar *msg){
  gnome_appbar_push(GNOME_APPBAR(statusbar), msg);
}
