/* Gnomba
 * Copyright (C) 1999 Gnomba Team
 *
 * net.c
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include <stdio.h>
#include <assert.h>
#include <signal.h>
#include "nmbhdr.h"
#include "net.h"
#include "error.h"
#include "smbwrap.h"
#include "smbscan.h"

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>


#define initSocks()
#define closeSocks()
typedef int SOCKET;
#define MAXSOCKETS 64
static u_int host_count = 0;

void sock_unblock(SOCKET s);
void sock_block(SOCKET s);
SOCKET add_sock(u_long ip, u_short port);
int getNMBInfo(u_long ip, char *machine, char *workgroup);
machineNode *gather(SOCKET *s, SOCKET max, int cnt, u_long ipstart, fd_set ofds,
		    struct timeval *tv, int retries, machineNode *curr);
machineNode *scanloop(char *strrange, long delay, machineNode *curr);


machineNode *netscan() 
{
  
  machineNode *root;
  machineNode *curr;
  int row, max;
  char *text;
  char key[15];

  root = malloc(sizeof(machineNode));

  /* So if we get nothing we don't crash, but does it cause other problems? */
  curr = root;
  curr->next = NULL;

  /* Test this now so we only show one warning */
  if ((BrokenLookup)&&(getuid()!=0)&&(geteuid()!=0)) { 
    ShowError("Must be root to bind to ports below 1024");
    return(root);
  }
 
	gnome_config_push_prefix("gnomba/");
	if (ScanType == 0) {
		curr = scan_neighborhood(curr);
  } else if (ScanType == 1) {
		g_print("WinsScanning not yet implemented");
	} else if (ScanType == 2) {
    max = gnome_config_get_int("Scan/Max=-1");
	
    for (row = 0; row < max; row++) {
      sprintf(key, "Scan/Range%d", row);
      text = gnome_config_get_string(key);
      if (debug)
        g_print("scanning %s\n", text);
      if (SlowScan) 
         curr = scanloop(text, 20000, curr);
      else curr = scanloop(text, 1000, curr);
    }
  }
  gnome_config_pop_prefix();

  return root;
}

machineNode *scanloop(char *strrange, long delay, machineNode * root)
{

  u_long ipstart, ipstop;
  u_char input[8];
  u_short i;
  int range;
  SOCKET max = 0;
  fd_set origfds;
  struct timeval tv =
  {0, 0};
  SOCKET *s;
  machineNode *curr;

  curr = root;
  initSocks();

#ifndef WIN32
  signal(SIGPIPE, SIG_IGN);
#endif

  sscanf(strrange, "%d.%d.%d.%d - %d.%d.%d.%d", (int *) &input[0], (int *) &input[1], (int *) &input[2], (int *) &input[3], (int *) &input[4], (int *) &input[5], (int *) &input[6], (int *) &input[7]);

  if (debug)
    printf("IP: %d.%d.%d.%d - %d.%d.%d.%d\n", input[0], input[1], input[2], input[3], input[4], input[5], input[6], input[7]);

  ipstart = (input[0] << 24) + (input[1] << 16) + (input[2] << 8) + input[3];
  ipstop = (input[4] << 24) + (input[5] << 16) + (input[6] << 8) + input[7];
  tv.tv_usec = delay;
  range = ipstop - ipstart + 1;
  if (range <= 0)
    return curr;
  s = (SOCKET *) g_malloc(sizeof(SOCKET) * ((range < MAXSOCKETS) ? range : MAXSOCKETS));

  if (s == NULL) {
    printf("Unable to alloc memory.\n");
    exit(-1);
  }
  bzero(s, sizeof(SOCKET) * ((range < MAXSOCKETS) ? range : MAXSOCKETS));

  do {
    FD_ZERO(&origfds);
    i = max = 0;
    for (; (i < range) && (i < MAXSOCKETS); ++i) {
      s[i] = add_sock(ipstart + i, 139);
      if (s[i] < 1) {
	continue;
      }
      max = (s[i] > max) ? s[i] : max;
      if (s)
	FD_SET(s[i], &origfds);
    }
    curr = gather(s, max, i, ipstart, origfds, &tv, 5, curr);

    range -= i;
    ipstart += i;
    for (; i > 0; --i)
      close(s[i]);
    close(s[0]);

  }
  while (range);

  closeSocks();
  g_free(s);
  return curr;
}

void sock_unblock(SOCKET s)
{
#ifdef WIN32
  u_long flag = 1;		/* (doblock ? 0 : 1) */
  ioctlsocket(s, FIONBIO, &flag);
#else
  int flag = O_NONBLOCK | fcntl(s, F_GETFL);
  fcntl(s, F_SETFL, flag);
#endif
}

void sock_block(SOCKET s)
{
#ifdef WIN32
  u_long flag = 0;		/* (doblock ? 0 : 1) */
  ioctlsocket(s, FIONBIO, &flag);
#else
  int flag = (~O_NONBLOCK) & fcntl(s, F_GETFL);
  fcntl(s, F_SETFL, flag);
#endif
}

SOCKET add_sock(u_long ip, u_short port)
{
  struct sockaddr_in dest;
  SOCKET sfd;
  struct linger slinger;
  int ret;

  bzero(&dest, sizeof(dest));

  sfd = socket(AF_INET, SOCK_STREAM, 0);
  while (sfd < 0) {
    usleep(10000);
    sfd = socket(AF_INET, SOCK_STREAM, 0);
  }

  slinger.l_onoff = 1;
  slinger.l_linger = 0;
  setsockopt(sfd, SOL_SOCKET, SO_LINGER, (char *) &slinger, sizeof(slinger));

  dest.sin_family = AF_INET;
  dest.sin_port = htons(port);
  dest.sin_addr.s_addr = htonl(ip);

  sock_unblock(sfd);
  ret = connect(sfd,(struct sockaddr *)&dest, sizeof(dest));
  if (ret < 0 && errno != EINPROGRESS) {
    close(sfd);
    return 0;
  }
  sock_block(sfd);
  return sfd;
}

machineNode *gather(SOCKET * s, SOCKET max, int cnt, u_long ipstart, 
                    fd_set ofds, struct timeval * tv, int retries, 
                    machineNode * root)
{
  fd_set wfds;
  int i, res, sel;
  u_char *ip;
  u_char buf[10];
  char nbt_name[25];
  char grp_name[25];
  machineNode *prev;
  machineNode *curr;

  struct timeval begin, end;
  long diff, timeout;

  curr = root;

  if (SlowScan) usleep(100000); else usleep(50000);

  timeout = tv->tv_sec + (tv->tv_usec * 100000);
  gettimeofday(&begin, NULL);
  sel = 1;

  while ((sel != 0) || (retries) || timeout) {
    wfds = ofds;
    sel = res = select(max + 1, NULL, &wfds, NULL, tv);
    if (!res) {
      --retries;
    }
    for (i = 0; res && i < cnt; ++i)
      if ((res > 0) && FD_ISSET(s[i], &wfds)) {
	if (send(s[i], buf, 0, 0) == 0) {
	  u_long currip = ipstart + i;
	  ip = (u_char *) & currip;
	  if (getNMBInfo(htonl(currip), nbt_name, grp_name) >= 0) {
	    if (debug)
	      printf("%d.%d.%d.%d --> %s\\\\%s\n", ip[3], ip[2], ip[1], ip[0], grp_name, nbt_name);

	    prev = curr;

	    curr = g_malloc(sizeof(machineNode));
	    curr->next = NULL;
              if ( DefaultUser && *DefaultUser )
                curr->username = g_strdup( DefaultUser );
              else
                curr->username = NULL;
	    curr->passwd = NULL;

	    curr->ip = g_strdup_printf("%d.%d.%d.%d", ip[3], ip[2], ip[1], ip[0]);

	    curr->machineName = g_strdup(nbt_name);
	    curr->workgroupName = g_strdup(grp_name);
	    curr->scanned = FALSE;
            curr->mountCreated = FALSE;
			
	    prev->next = curr;

	    host_count++;
	  } else {
	    if (debug)
	      g_print("%d.%d.%d.%d <NoName>\n", ip[3], ip[2], ip[1], ip[0]);
	    host_count++;
	  }
      } 
      FD_CLR(s[i], &ofds);
      close(s[i]);
      --res;
    }
    gettimeofday(&end, NULL);
    diff = (end.tv_sec + (100000 * end.tv_usec)) - (begin.tv_sec + (100000 * begin.tv_usec));
    timeout -= timeout;
  }

  return (curr);
}



int getNMBInfo(u_long ip, char *machine, char *workgroup)
{

  struct sockaddr_in sin_dst, sin_src;
  struct nmbhdr *nmb;
  char *data;
  struct typez *typz;
  char buffer[1024];
  int socket_client, i, timeout = 0;
  int longueur = sizeof(struct sockaddr_in);
  int count;
	int loops;
	int optval;
  struct _nameinfo {
    char name[15];
    char type;
    char res;
    char fill;
  } *ninf;


  bzero(buffer, sizeof(buffer));
  nmb = (struct nmbhdr *) buffer;
  data = (char *) (buffer + NMBHDRSIZE);
  typz = (struct typez *) (buffer + NMBHDRSIZE + 33);

  sin_dst.sin_family = AF_INET;
  sin_dst.sin_port = htons(137);
  sin_dst.sin_addr.s_addr = ip;

  memcpy(data, "CKAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 34);

  nmb->id = 0x600;
  nmb->R = 0;			/* 0 for question 1 for response */
  nmb->opcode = 0;		/* 0 = query */
  nmb->que_num = htons(1);	/* i have only 1 question :) */
  nmb->namelen = 0x20;
  typz->type = 0x2100;
  typz->type2 = 0x0100;

  socket_client = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

  if (BrokenLookup) {
	  optval=1;
		setsockopt(socket_client,SOL_SOCKET,SO_REUSEADDR,&optval,sizeof(optval));
		sin_src.sin_port = htons (137);
		sin_src.sin_addr.s_addr = INADDR_ANY;
		if (bind(socket_client,(struct sockaddr *) &sin_src,sizeof(sin_src))==(-1))
		  {
	      perror("bind");
			  return(-1);
			}
	}
	
	
  sendto(socket_client, buffer, 50, 0, (struct sockaddr *) &sin_dst, longueur);
  usleep(100000);
  sendto(socket_client, buffer, 50, 0, (struct sockaddr *) &sin_dst, longueur);

  bzero(buffer, sizeof(buffer));
  nmb = (struct nmbhdr *) (buffer);
  machine[0] = '\0';
  workgroup[0] = '\0';

  sock_unblock(socket_client);

	if (SlowScan) loops = 50; else loops = 2;
  for (timeout = 0; timeout < loops; timeout++) {
    if (SlowScan) usleep(700000); else usleep(100000);
    if (recvfrom(socket_client, buffer, sizeof(buffer), 0, (struct sockaddr *) &sin_dst, &(int) longueur) != -1) {
      if (nmb->rep_num != 0) {

	/*set to first nameblock after header */
	count = 0;
	ninf = (struct _nameinfo *) (buffer + 57 + (count++ * sizeof(struct _nameinfo)));
	while ((machine[0] == '\0') && (57 + (count * sizeof(struct _nameinfo)) < 1024)) {
	  if (ninf->type == 0) {
	    i = 0;
	    while ((i < 15)) {
	      machine[i] = ninf->name[i];
	      i++;
	    }
	    machine[i] = '\0';
	    machine = g_strchomp(machine);
	  }
	  ninf = (struct _nameinfo *) (buffer + 57 + (count++ * sizeof(struct _nameinfo)));
	}
	if (machine[0] == '\0') {
	  if (debug)
	    printf("we finished without finding anything, giving up\n");
	  close(socket_client);
	  return (-1);
	}
	while ((workgroup[0] == '\0') && (57 + (count * sizeof(struct _nameinfo)) < 1024)) {
	  if (ninf->type == 0) {
	    i = 0;
	    while ((i < 15)) {
	      workgroup[i] = ninf->name[i];
	      i++;
	    }
	    workgroup[i] = '\0';
	    workgroup = g_strchomp(workgroup);
	  }
	  ninf = (struct _nameinfo *) (buffer + 57 + (count++ * sizeof(struct _nameinfo)));
	}
	if (workgroup[0] == '\0') {
	  if (debug)
	    printf("we finished without finding anything, giving up\n");
	  close(socket_client);
	  return (-1);
	}
	close(socket_client);
	return 0;
      }
      close(socket_client);
      return (-1);
    }
  }
  close(socket_client);
  return (-1);
}

/*
 * Local Variables:
 * mode:C
 * c-indent-level:2
 * End:
 */
