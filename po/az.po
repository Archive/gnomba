# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2001 Free Software Foundation, Inc.
# Rüstəm Əsgərov <rustam@boun.edu.tr>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: gnomba\n"
"POT-Creation-Date: 2001-03-31 07:06+0200\n"
"PO-Revision-Date: 2001-03-12 15:18+0200\n"
"Last-Translator: Rüstəm Əsgərov <rustam@boun.edu.tr>\n"
"Language-Team: Azerbaijani Turkish\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/browser.c:102
msgid "Find Dialog"
msgstr "Dialoqu tap"

#: src/browser.c:111
msgid "Find:"
msgstr "Tap:"

#. create window which prompts for username and password
#: src/browser.c:200
msgid "Authentication"
msgstr "Tanıtma"

#: src/browser.c:211 src/browser.c:604
msgid "Username:"
msgstr "Istifadəçi adı:"

#: src/browser.c:229
msgid "password:"
msgstr "parol:"

#: src/browser.c:246
msgid "workgroup:"
msgstr "işqrupu:"

#. create and launch window to prompt for mount point.
#. set to call updateMount on "OK"
#. create window which prompts for mount point username and password
#. ok button will call updatePW
#: src/browser.c:317 src/browser.c:558
msgid "Select Mount Point"
msgstr "Bağlama nögtəsini seç"

#: src/browser.c:388 src/browser.c:457
msgid "Error creating directory"
msgstr "Qovluq yaratma xətası"

#: src/browser.c:399
#, c-format
msgid "Directory %s doesn't exist ! Create it ?"
msgstr "%s qovluğu mövcud deyildir! Yaradım ?"

#: src/browser.c:463
msgid "The mount point you specify is not a directory"
msgstr "Bildirdiyiniz bağlama nögtəsi qovluq  deyildir"

#: src/browser.c:466
msgid "Too many symbolic links in the path"
msgstr "Izdə çoxlu simvolik körpü var"

#: src/browser.c:469
msgid "You don't have enough privileges to access this directory"
msgstr "Bu qovluğa daxil olmaq üçün icazəniz yoxdur"

#: src/browser.c:472
msgid "The directory name is too long"
msgstr "Qovluq adı çox uzundur"

#: src/browser.c:475
msgid "Unknown Error With Directory"
msgstr "Qovluqda bilinməyən xəta"

#: src/browser.c:512
#, c-format
msgid "mounted on:>%s<\n"
msgstr "bağlanma yeri:>%s<\n"

#: src/browser.c:569
msgid "Mount Point:"
msgstr "Bağlama nögtəsi"

#: src/browser.c:593
msgid "Provide share level authentication"
msgstr "Bölüşmə səviyyəsi təsdiqləməsini bildirin"

#: src/browser.c:621
msgid "Password:"
msgstr "Parol:"

#: src/browser.c:751
msgid "Browse In GMC"
msgstr "GMC-də dara"

#: src/browser.c:751
msgid "Browse the current share in gmc"
msgstr "Hazırki bölüşməni gmc-də dara"

#: src/browser.c:752
msgid "Mount"
msgstr "Bağla"

#: src/browser.c:753
msgid "Mount with command"
msgstr "Əmrlə bağla"

#: src/browser.c:753
msgid "Mount with default command"
msgstr "Ön qurğulu əmrlə bağla"

#: src/browser.c:759
msgid "UnMount"
msgstr "Ayır"

#: src/browser.c:765
msgid "Add as network printer"
msgstr "Şəbəkə çap edicisi kimi əlavə et"

#: src/browser.c:765
msgid "Add the printer as a network printer"
msgstr "Çap edicini şəbəkə çap edicisi kimi əlavə et"

#: src/browser.c:766
msgid "Add as SMB printer"
msgstr "SMB çap edicisi kimi əlavə et"

#: src/browser.c:766
msgid "Add as a samba printer"
msgstr "Samba çap edicisi kimi əlavə et"

#: src/browser.c:767 src/properties.c:650
msgid "Remove"
msgstr "Sil"

#: src/browser.c:767
msgid "Remove printer"
msgstr "Çap edicini sil"

#: src/browser.c:773
msgid "Refresh"
msgstr "Təzələ"

#: src/browser.c:773
msgid "Refresh Share List"
msgstr "Bölüşmə siyahısını təzələ"

#: src/browser.c:774 src/browser.c:780
msgid "Sort"
msgstr "Sırala"

#: src/browser.c:774 src/browser.c:780
msgid "Sort List"
msgstr "Siyahını sırala"

#: src/browser.c:866
msgid "Workgroup Name"
msgstr "Işgrupu Adı"

#: src/browser.c:867
msgid "Comment"
msgstr "Təfsir"

#: src/browser.c:868
msgid "Mount Point"
msgstr "Bağlama nögtəsi"

#: src/gnomba.c:102 src/menu.c:107
msgid "gnomba"
msgstr "gnomba"

#. copyrigth notice
#: src/menu.c:110
msgid "(C) 1999, 2000"
msgstr "(C) 1999,2000"

#. another comments
#: src/menu.c:113
msgid "Gnome Samba Browser"
msgstr "Gnome Samba Darayıcısı"

#: src/menu.c:122
msgid "(re)_Scan"
msgstr "(yenidən)_Dara"

#: src/menu.c:122
msgid "Scan"
msgstr "Dara"

#: src/menu.c:135
msgid "_Expand Workgroups"
msgstr "Işqrupunu _genişləndir"

#: src/menu.c:135
msgid "Expand all workgroups"
msgstr "Bütün işqruplarını genişləndir"

#: src/menu.c:142
msgid "_Collapse Workgroups"
msgstr "Işqruplarını _yığ"

#: src/menu.c:142
msgid "Collapse all workgroups"
msgstr "Bütün işqruplarını yığ"

#: src/menu.c:149
msgid "_Find"
msgstr "_Tap"

#: src/menu.c:149
msgid "Find"
msgstr "Tap"

#: src/menu.c:159
msgid "_Browse in GMC"
msgstr "GMC-də _dara"

#: src/menu.c:159
msgid "Browse the current share with GMC"
msgstr "GMC-lə hazırki bölüşməni dara"

#: src/menu.c:166
msgid "_Mount share"
msgstr "Bölüşməni _bağla"

#: src/menu.c:166
msgid "Mount the current share"
msgstr "Hazırki bölüşməni bağla"

#: src/menu.c:173
msgid "_Mount share with command"
msgstr "Bölüşməni əmrlə _bağla"

#: src/menu.c:173
msgid "Mount the current share and execute default command"
msgstr "Hazırki bölüşməni bağla və ön qurğulu əmri icra et"

#: src/menu.c:180
msgid "_Unmount share"
msgstr "Bölüşməni _ayır"

#: src/menu.c:180
msgid "Unmount the current share"
msgstr "Hazırki bölüşməni ayır"

#: src/menu.c:187
msgid "Unmount _All"
msgstr "_Hamısını ayır"

#: src/menu.c:187
msgid "Unmount all shares"
msgstr "Bütün bölüşmələri ayır"

#: src/menu.c:214
msgid "_Action"
msgstr "_Fəliyyət"

#: src/properties.c:140
msgid "Font selection ..."
msgstr "Yazı növü seçimi ..."

#: src/properties.c:462
msgid "Gnomba Preferences"
msgstr "Gnomba Xüsusiyyətləri"

#. set these up here so they can be set inactive by the radiobutton
#: src/properties.c:475
msgid "IP Scan options"
msgstr "IP daraması seçənəkləri"

#: src/properties.c:476
msgid "WINS Server options"
msgstr "WINS vericisi seçənəkləri"

#. ** Scan using Samba's Master Browser concept**
#: src/properties.c:480 src/properties.c:516
msgid "Scan using SMB protocol"
msgstr "SMB protokoluyla dara"

#. ** Scan using a WINS server **
#: src/properties.c:490
msgid "Scan using WINS protocol (not yet implemented)"
msgstr "WINS protokoluyla dara(hələ mövcud deyil)"

#. ** Scan using our ip scanning method **
#: src/properties.c:502
msgid "IP Scan method"
msgstr "IP darama üsulu"

#: src/properties.c:518
msgid "Scan Range"
msgstr "Darama genişliyi"

#. to and from labels and entries
#: src/properties.c:540
msgid "From:"
msgstr "Yollayan:"

#: src/properties.c:569
msgid "To:"
msgstr "Yollanan:"

#: src/properties.c:644
msgid "Add"
msgstr "Əlavə et"

#: src/properties.c:656
msgid "Update"
msgstr "Yenilə"

#: src/properties.c:667
msgid "Scan Method"
msgstr "Darama üsulu"

#: src/properties.c:695
msgid "Scan on program open"
msgstr "Proqram açılanda dara"

#: src/properties.c:709
msgid "Hide IPC$ shares"
msgstr "IPS$ bölüşmələrini gizlə"

#: src/properties.c:720
msgid "Hide all $ shares"
msgstr "Bütün $ bölüşmələrini gizlə"

#: src/properties.c:732
msgid "Silently create non existant mount point directories?"
msgstr "Olmayan bağlama nöqtələrini səssiz yaradım?"

#: src/properties.c:744
msgid "Don't remove created mount point directories?"
msgstr "Qurulan bağlama nögtəsi govluğlarını silməyim?"

#: src/properties.c:753
msgid "Options"
msgstr "Seçənəklər"

#: src/properties.c:768
msgid "Net Font"
msgstr "Şəbəkə yazı növü"

#: src/properties.c:776 src/properties.c:797 src/properties.c:818
msgid "Select font ..."
msgstr "Yazı növünü seç ..."

#: src/properties.c:789
msgid "Comment Font"
msgstr "Təfsir yazı növü"

#: src/properties.c:810
msgid "Mount Font"
msgstr "Bağlama yazı növünü"

#: src/properties.c:829
msgid "Font Selection"
msgstr "Yazı növü seçimi"

#: src/properties.c:840
msgid "Default User"
msgstr "Ön qurğulu istifadəçi"

#: src/properties.c:854
msgid "Default Workgroup"
msgstr "Ön qurğulu işqrupu"

#: src/properties.c:868
msgid "Dont ask for authentication if no guest shares"
msgstr "Qonaq bölüşmələri yoxsa tanıtmanı istəmə"

#: src/properties.c:877
msgid "Credentials"
msgstr "E'timadnamə"

#: src/properties.c:888
msgid "Default Command:"
msgstr "Ön qurğulu əmr:"

#: src/properties.c:908
msgid "Auto mount on doubleclick"
msgstr "Cüt basmada avtomatik bağla"

#: src/properties.c:923
msgid "Default root for mount point:"
msgstr "Bağlama nögtəsi üçün ön qurğulu root:"

#. Run command on mount
#: src/properties.c:937
msgid "Run command on automount"
msgstr "Avtobağlamada əmri icra et"

#: src/properties.c:959
msgid "Command Settings"
msgstr "Əmr dəyişiklikləri"

#: src/properties.c:970
msgid "Write debug info to tty"
msgstr "Xəta ayıqlaması mə'lumatını tty-ya yaz"

#: src/properties.c:981
msgid "Using pre 2.0.5a samba"
msgstr "2.0.a samba istifadə edirəm"

#: src/properties.c:992
msgid "Using samba 2.0.6 or greater"
msgstr "samba 2.0.6 və ya daha yüksək istifadə edirəm"

#: src/properties.c:1003
msgid "Evil 95 workaround (requires root privleges)"
msgstr "Evil 95 workaround (root olmağınızı tələb edir)"

#: src/properties.c:1014
msgid "Increased Scan Timeouts"
msgstr "Artmış Darama Vaxt Dolmaları"

#: src/properties.c:1023
msgid "Advanced Options"
msgstr "Üstün seçənəklər"

#: src/smbwrap.c:66 src/smbwrap.c:72 src/smbwrap.c:348 src/smbwrap.c:353
msgid "Error creating pipes"
msgstr "Boru yaratma xətası"

#: src/smbwrap.c:83
msgid "Error Setting up /dev/tty"
msgstr "/dev/tty qurma xətası"

#. This return value seems to indicate that we couldn't find it
#: src/smbwrap.c:127
msgid ""
"smbclient was not found. Make sure you have it\n"
"installed and it is in your path"
msgstr ""
"smbclient tapılmadı.Qurulu olduğundan və\n"
"sizin izdə olduğundan əmin olun"

#. This seems to mean the perms are wrong
#: src/smbwrap.c:131
msgid ""
"Could not execute smbclient. Make sure it has\n"
"correct permissions for you to execute it"
msgstr ""
"smbclient-i icra edə bilmədim.Siz tərəfindən icra edilməsi üçün\n"
"sizdə doğru icazələrin oldğundan əmin olun"

#. fix stdio
#. This return value seems to indicate that we couldn't find it
#: src/smbwrap.c:378
msgid ""
"smbmount was not found. Make sure you have it\n"
"installed and it is in your path"
msgstr ""
"smbmount tapılmadı.Qurulu olduğundan və\n"
"sizin izdə olduğundan əmin olun"

#. This seems to mean the perms are wrong
#: src/smbwrap.c:382
msgid ""
"Could not execute smbmount. Make sure it has\n"
"correct permissions for you to execute it"
msgstr ""
"smbmount-u icra edə bilmədim.Siz tərəfindən icra edilməsi üçün\n"
"sizdə doğru icazələrin oldğundan əmin olun"

#: src/smbwrap.c:488
msgid ""
"Error unmounting, please make sure that you have smbumount installed\n"
"and that it is suid root if you are a normal user"
msgstr ""
"Ayırma xətası,smbumount qurulmuş olduğundan və\n"
"ali istifadəçi olduğunuzdan əmin olun"

#: src/smbwrap.c:536
msgid "addNetworkPrinter: Sorry ! Not yet implemented !"
msgstr "Şəbəkəyzıcısınıəlavəet: Hələ mövcud deyil!"

#: src/smbwrap.c:545
msgid "addSmbPrinter: Sorry ! Not yet implemented !"
msgstr "Smbyazıçısınıəlavəet: Hələ mövcud deyil!"

#: src/smbwrap.c:554
msgid "removePrinter: Sorry ! Not yet implemented !"
msgstr "yazıçınıaradangötür: Hələ mövcud deyil!"

#: src/smbwrap.c:591
msgid ""
"Before scanning you must specify an IP range to scan.\n"
"This is done via the Options|Preferences menu, on the Configuration tab."
msgstr ""
"Daramagdan əvvəl IP darama genişliyini bildirməlisiniz.\n"
"Bunu Quraşdırma-da , Seçənəklər|Üstünlüklər-də edə bilərsiz. "

#: src/smbwrap.c:596
msgid "scanning..."
msgstr "darayıram..."

#: src/smbwrap.c:611
msgid "Network"
msgstr "Şəbəkə"
