# gnomba sk.po
# Copyright (C) 2001 Free Software Foundation, Inc.
# Marcel Telka <marcel@telka.sk>, 2001.
#
msgid ""
msgstr ""
"Project-Id-Version: gnomba\n"
"POT-Creation-Date: 2001-09-24 08:25+0200\n"
"PO-Revision-Date: 2001-09-24 08:24+0200\n"
"Last-Translator: Marcel Telka <marcel@telka.sk>\n"
"Language-Team: Slovak <sk-i18n@lists.linux.sk>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=iso-8859-2\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/browser.c:102
msgid "Find Dialog"
msgstr "Dial�gov� okno vyh�ad�vania"

#: src/browser.c:111
msgid "Find:"
msgstr "H�ada�:"

#. create window which prompts for username and password
#: src/browser.c:200
msgid "Authentication"
msgstr "Overenie toto�nosti"

#. Create the username prompt and edit box
#.
#: src/browser.c:211 src/browser.c:604
msgid "Username:"
msgstr "Prihlasovacie meno:"

#: src/browser.c:229
msgid "password:"
msgstr "heslo:"

#: src/browser.c:246
msgid "workgroup:"
msgstr "pracovn� skupina:"

#. create and launch window to prompt for mount point.
#. set to call updateMount on "OK"
#. create window which prompts for mount point username and password
#. ok button will call updatePW
#: src/browser.c:317 src/browser.c:558
msgid "Select Mount Point"
msgstr "Vybra� miesto pripojenia"

#: src/browser.c:388 src/browser.c:457
msgid "Error creating directory"
msgstr "Chyba pri vytv�ran� adres�ra"

#: src/browser.c:399
#, c-format
msgid "Directory %s doesn't exist ! Create it ?"
msgstr "Adres�r %s neexistuje! Vytvori� ho?"

#: src/browser.c:463
msgid "The mount point you specify is not a directory"
msgstr "Miesto pripojenia, ktor� ste zadali neexistuje"

#: src/browser.c:466
msgid "Too many symbolic links in the path"
msgstr "Pr�li� ve�a symbolick�ch linieek v ceste"

#: src/browser.c:469
msgid "You don't have enough privileges to access this directory"
msgstr "Nem�te potrebn� pr�va na pr�stup k tomuto adres�ru"

#: src/browser.c:472
msgid "The directory name is too long"
msgstr "N�zov adres�ra je pr�li� dlh�"

#: src/browser.c:475
msgid "Unknown Error With Directory"
msgstr "Nezn�ma chyba v adres�ri"

#: src/browser.c:512
#, c-format
msgid "mounted on:>%s<\n"
msgstr "pripojen� na:>%s<\n"

#: src/browser.c:569
msgid "Mount Point:"
msgstr "Miesto pripojenia:"

#: src/browser.c:593
msgid "Provide share level authentication"
msgstr "Poskytova� zdie�an� �rove� autentifik�cie"

#: src/browser.c:621
msgid "Password:"
msgstr "Heslo:"

#: src/browser.c:751
msgid "Browse In GMC"
msgstr "Prezera� v GMC"

#: src/browser.c:751
msgid "Browse the current share in gmc"
msgstr "Prezera� aktu�lne zdie�an� v gmc"

#: src/browser.c:752
msgid "Mount"
msgstr "Pripoji�"

#: src/browser.c:753
msgid "Mount with command"
msgstr "Pripoji� s pr�kazom"

#: src/browser.c:753
msgid "Mount with default command"
msgstr "Pripoji� s predvolen�m pr�kazom"

#: src/browser.c:759
msgid "UnMount"
msgstr "Odpoji�"

#: src/browser.c:765
msgid "Add as network printer"
msgstr "Prida� ako sie�ov� tla�iare�"

#: src/browser.c:765
msgid "Add the printer as a network printer"
msgstr "Prida� tla�iare� ako sie�ov� tla�iare�"

#: src/browser.c:766
msgid "Add as SMB printer"
msgstr "Prida� ako tla�iare� SMB"

#: src/browser.c:766
msgid "Add as a samba printer"
msgstr "Prida� ako tla�iare� samba"

#: src/browser.c:767 src/properties.c:650
msgid "Remove"
msgstr "Odobra�"

#: src/browser.c:767
msgid "Remove printer"
msgstr "Odobra� tla�iare�"

#: src/browser.c:773
msgid "Refresh"
msgstr "Obnovi�"

#: src/browser.c:773
msgid "Refresh Share List"
msgstr "Obnovi� zoznam zdie�an�ch"

#: src/browser.c:774 src/browser.c:780
msgid "Sort"
msgstr "Triedi�"

#: src/browser.c:774 src/browser.c:780
msgid "Sort List"
msgstr "Utriedi� zoznam"

#: src/browser.c:866
msgid "Workgroup Name"
msgstr "N�zov pracovnej skupiny"

#: src/browser.c:867
msgid "Comment"
msgstr "Koment�r"

#: src/browser.c:868
msgid "Mount Point"
msgstr "Miesto pripojenia"

#: src/gnomba.c:102 src/menu.c:107
msgid "gnomba"
msgstr "gnomba"

#. copyrigth notice
#: src/menu.c:110
msgid "(C) 1999, 2000"
msgstr "(C) 1999, 2000"

#. another comments
#: src/menu.c:113
msgid "Gnome Samba Browser"
msgstr "Gnome Samba prehliada�"

#: src/menu.c:122
msgid "(re)_Scan"
msgstr "_Skenova�"

#: src/menu.c:122
msgid "Scan"
msgstr "Skenova�"

#: src/menu.c:135
msgid "_Expand Workgroups"
msgstr "R_ozbali� pracovn� skupiny"

#: src/menu.c:135
msgid "Expand all workgroups"
msgstr "Rozbali� v�etky pracovn� skupiny"

#: src/menu.c:142
msgid "_Collapse Workgroups"
msgstr "Zbali� pra_covn� skupiny"

#: src/menu.c:142
msgid "Collapse all workgroups"
msgstr "Zbali� v�etky pracovn� skupiny"

#: src/menu.c:149
msgid "_Find"
msgstr "_H�ada�"

#: src/menu.c:149
msgid "Find"
msgstr "H�ada�"

#: src/menu.c:159
msgid "_Browse in GMC"
msgstr "_Prezera� v GMC"

#: src/menu.c:159
msgid "Browse the current share with GMC"
msgstr "Prezera� aktu�lne zdie�an� v GMC"

#: src/menu.c:166
msgid "_Mount share"
msgstr "_Pripoji� zdie�an�"

#: src/menu.c:166
msgid "Mount the current share"
msgstr "Pripoji� aktu�lne zdie�an�"

#: src/menu.c:173
msgid "_Mount share with command"
msgstr "_Pripoji� zdie�an� s pou�it�m pr�kazu"

#: src/menu.c:173
msgid "Mount the current share and execute default command"
msgstr "Pripoji� aktu�lne zdie�an� a vykona� predvolen� pr�kaz"

#: src/menu.c:180
msgid "_Unmount share"
msgstr "_Odpoji� zdie�an�"

#: src/menu.c:180
msgid "Unmount the current share"
msgstr "Odpoji� aktu�lne zdie�an�"

#: src/menu.c:187
msgid "Unmount _All"
msgstr "Odpoji� _v�etky"

#: src/menu.c:187
msgid "Unmount all shares"
msgstr "Odpoji� v�etky zdie�an�"

#: src/menu.c:214
msgid "_Action"
msgstr "_Akcia"

#: src/properties.c:140
msgid "Font selection ..."
msgstr "V�ber typu p�sma..."

#: src/properties.c:462
msgid "Gnomba Preferences"
msgstr "Nastavenia programu Gnomba"

#. set these up here so they can be set inactive by the radiobutton
#: src/properties.c:475
msgid "IP Scan options"
msgstr "Mo�nosti IP skenovania"

#: src/properties.c:476
msgid "WINS Server options"
msgstr "Vo�by pre server WINS"

#. ** Scan using Samba's Master Browser concept**
#: src/properties.c:480 src/properties.c:516
msgid "Scan using SMB protocol"
msgstr "Skenova� s pou�it�m protokolu SMB"

#. ** Scan using a WINS server **
#: src/properties.c:490
msgid "Scan using WINS protocol (not yet implemented)"
msgstr "Skenova� s pou�it�m protokolu WINS (zatia� neimplementovan�)"

#. ** Scan using our ip scanning method **
#: src/properties.c:502
msgid "IP Scan method"
msgstr "Met�da IP skenovania"

#: src/properties.c:518
msgid "Scan Range"
msgstr "Rozsah skenovania"

#. to and from labels and entries
#: src/properties.c:540
msgid "From:"
msgstr "Od:"

#: src/properties.c:569
msgid "To:"
msgstr "Do:"

#: src/properties.c:644
msgid "Add"
msgstr "Prida�"

#: src/properties.c:656
msgid "Update"
msgstr "Aktualizova�"

#: src/properties.c:667
msgid "Scan Method"
msgstr "Met�da skenovania"

#: src/properties.c:695
msgid "Scan on program open"
msgstr "Skenova� pri otvoren� programu"

#: src/properties.c:709
msgid "Hide IPC$ shares"
msgstr "Skry� zdie�an� IPC$"

#: src/properties.c:720
msgid "Hide all $ shares"
msgstr "Skry� v�etky zdie�an� $"

#: src/properties.c:732
msgid "Silently create non existant mount point directories?"
msgstr "Potichu vytvori� neexistuj�ce adres�r pre pr�pojn� bod?"

#: src/properties.c:744
msgid "Don't remove created mount point directories?"
msgstr "Neodstr�ni� vytvoren� adres�r pre pr�pojn� bod?"

#: src/properties.c:753
msgid "Options"
msgstr "Vo�by"

#: src/properties.c:768
msgid "Net Font"
msgstr "Typ p�sma siete"

#: src/properties.c:776 src/properties.c:797 src/properties.c:818
msgid "Select font ..."
msgstr "Vybra� typ p�sma..."

#: src/properties.c:789
msgid "Comment Font"
msgstr "Typ p�sma koment�ra"

#: src/properties.c:810
msgid "Mount Font"
msgstr "Typ p�sma pripojenia"

#: src/properties.c:829
msgid "Font Selection"
msgstr "V�ber typu p�sma"

#: src/properties.c:840
msgid "Default User"
msgstr "Predvolen� pou��vate�"

#: src/properties.c:854
msgid "Default Workgroup"
msgstr "Predvolen� pracovn� skupina"

#: src/properties.c:868
msgid "Dont ask for authentication if no guest shares"
msgstr "Nep�ta� sa na autentifik�ciu ak nie s� zdie�an� pre hos�a"

#: src/properties.c:877
msgid "Credentials"
msgstr "Poverenia"

#: src/properties.c:888
msgid "Default Command:"
msgstr "Predvolen� pr�kaz:"

#: src/properties.c:908
msgid "Auto mount on doubleclick"
msgstr "Automaticky pripoji� pri dvojitom kliknut�"

#: src/properties.c:923
msgid "Default root for mount point:"
msgstr "Predvolen� kore� pre bod pripojenia:"

#. Run command on mount
#: src/properties.c:937
msgid "Run command on automount"
msgstr "Spusti� pr�kaz pri automatickom pripojen�"

#: src/properties.c:959
msgid "Command Settings"
msgstr "Nastavenia pr�kazu"

#: src/properties.c:970
msgid "Write debug info to tty"
msgstr "Zap�sa� ladiace inform�cie na tty"

#: src/properties.c:981
msgid "Using pre 2.0.5a samba"
msgstr "Pou��van� samba pred verziou 2.0.5a"

#: src/properties.c:992
msgid "Using samba 2.0.6 or greater"
msgstr "Pou��van� samba verzie 2.0.6 alebo vy��ia"

#: src/properties.c:1003
msgid "Evil 95 workaround (requires root privleges)"
msgstr "Diabolsk� obch�dzka pre 95 (vy�aduje pr�va spr�vcu)"

#: src/properties.c:1014
msgid "Increased Scan Timeouts"
msgstr "Zv��en� �asov� limit pre skenovanie"

#: src/properties.c:1023
msgid "Advanced Options"
msgstr "Roz��ren� nastavenia"

#: src/smbwrap.c:66 src/smbwrap.c:72 src/smbwrap.c:348 src/smbwrap.c:353
msgid "Error creating pipes"
msgstr "Chyba pri vytv�ran� r�r"

#: src/smbwrap.c:83
msgid "Error Setting up /dev/tty"
msgstr "Chyba pri nastaven� /dev/tty"

#. This return value seems to indicate that we couldn't find it
#: src/smbwrap.c:127
msgid ""
"smbclient was not found. Make sure you have it\n"
"installed and it is in your path"
msgstr ""
"Nebol n�jden� smbclient. Overte, �i ho m�te\n"
"nain�talovan� a �i je vo va�ej ceste"

#. This seems to mean the perms are wrong
#: src/smbwrap.c:131
msgid ""
"Could not execute smbclient. Make sure it has\n"
"correct permissions for you to execute it"
msgstr ""
"Nem��em spusti� smbclient. Overte, �i m�te\n"
"dostato�n� pr�va na jeho spustenie"

#. fix stdio
#. This return value seems to indicate that we couldn't find it
#: src/smbwrap.c:378
msgid ""
"smbmount was not found. Make sure you have it\n"
"installed and it is in your path"
msgstr ""
"Nebol n�jden� smbmount. Overte, �e ho m�te\n"
"nain�talovan� a �i je vo va�ej ceste"

#. This seems to mean the perms are wrong
#: src/smbwrap.c:382
msgid ""
"Could not execute smbmount. Make sure it has\n"
"correct permissions for you to execute it"
msgstr ""
"Nem��em spusti� smbmount. Overte, �i m�te\n"
"dostato�n� pr�va na jeho spustenie"

#: src/smbwrap.c:488
msgid ""
"Error unmounting, please make sure that you have smbumount installed\n"
"and that it is suid root if you are a normal user"
msgstr ""
"Chyba pri odmontovan�. Overte, pros�m, �i m�te nain�talovan� smbmount\n"
"a �i je suid root, ak ste oby�ajn� pou��vate�"

#: src/smbwrap.c:536
msgid "addNetworkPrinter: Sorry ! Not yet implemented !"
msgstr "addNetworkPrinter: Prep��te! Zatia�, nie je implementovan�!"

#: src/smbwrap.c:545
msgid "addSmbPrinter: Sorry ! Not yet implemented !"
msgstr "addSmbPrinter: Prep��te! Zatia�, nie je implementovan�!"

#: src/smbwrap.c:554
msgid "removePrinter: Sorry ! Not yet implemented !"
msgstr "removePrinter: Prep��te! Zatia�, nie je implementovan�!"

#: src/smbwrap.c:591
msgid ""
"Before scanning you must specify an IP range to scan.\n"
"This is done via the Options|Preferences menu, on the Configuration tab."
msgstr ""
"Pred skenovan�m mus�te zada� rozsah IP adries na skenovanie.\n"
"M��ete tak urobi� na z�lo�ke Konfigur�cia v menu Nastavenia|Predvo�by."

#: src/smbwrap.c:596
msgid "scanning..."
msgstr "skenujem..."

#: src/smbwrap.c:611
msgid "Network"
msgstr "Sie�"
